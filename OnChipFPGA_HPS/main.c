#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"
#include <math.h>

//#include "soc_cv_av/socal/socal.h"
//#include "soc_cv_av/socal/hps.h"
//#include "soc_cv_av/socal/alt_gpio.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"

#include "HPS_OnChip.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

#define ALT_AXI_FPGASLVS_OFST (0xC0000000) // axi_master
#define HW_FPGA_AXI_SPAN (0x40000000) // Bridge span 1GB
#define HW_FPGA_AXI_MASK ( HW_FPGA_AXI_SPAN - 1 )

uint16_t buffer[480][640] = { 0 };//{0xff, 0xff, 0xff}, {0x00, 0x00, 0xff}, {0x00, 0xff, 0x00}, {0xff, 0x00, 0x00} };

void *LEDs;
void *onChipMemory;


int main(int argc, char* argv[])
{
	void* 	axi_virtual_base;
	
	int 	fd;
	
	if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		printf("ERROR: could not open \"/dev/mem\"...\n");
		
		return 1;
	}

	axi_virtual_base = mmap( NULL, HW_FPGA_AXI_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, ALT_AXI_FPGASLVS_OFST );
	
	
	if (axi_virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}
	
	LEDs = axi_virtual_base + ((unsigned long)(H2F_LEDS_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	onChipMemory = axi_virtual_base + ((unsigned long)(ONCHIP_MEMORY_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	
	int a, b, c;
	while(1)
	{	
		printf("Za citanje unesi 0, za pisanje 1, za LED 2.\n");
		scanf("%d", &c);
		switch(c)
		{
			case 0:
				printf("Unesi offset:\n");
				scanf("%d", &a);

				printf("Procitana vrijednost je: %d\n", *((uint8_t*)onChipMemory + a));
				break;
			case 1:
				printf("Unesi offset:\n");
				scanf("%d", &a);
				printf("Unesi vrijednost:\n");
				scanf("%d", &b);
				*((uint8_t*)onChipMemory + a) = (uint8_t)b;
				break;
			case 2:
				printf("Unesi vrijednost:\n");
				scanf("%d", &b);
				*((uint8_t*)LEDs) = b;
				break;
		}
		
	}
	

	if (munmap(axi_virtual_base, HW_FPGA_AXI_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}

	close(fd);

	return 0;
}
