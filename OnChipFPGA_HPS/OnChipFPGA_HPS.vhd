

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity OnChipInterface is
	port(
		CLK_50: in std_logic;
		hps_0_ddr_mem_a       : out   std_logic_vector(14 downto 0);                    -- mem_a
		hps_0_ddr_mem_ba      : out   std_logic_vector(2 downto 0);                     -- mem_ba
		hps_0_ddr_mem_ck      : out   std_logic;                                        -- mem_ck
		hps_0_ddr_mem_ck_n    : out   std_logic;                                        -- mem_ck_n
		hps_0_ddr_mem_cke     : out   std_logic;                                        -- mem_cke
		hps_0_ddr_mem_cs_n    : out   std_logic;                                        -- mem_cs_n
		hps_0_ddr_mem_ras_n   : out   std_logic;                                        -- mem_ras_n
		hps_0_ddr_mem_cas_n   : out   std_logic;                                        -- mem_cas_n
		hps_0_ddr_mem_we_n    : out   std_logic;                                        -- mem_we_n
		hps_0_ddr_mem_reset_n : out   std_logic;                                        -- mem_reset_n
		hps_0_ddr_mem_dq      : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
		hps_0_ddr_mem_dqs     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
		hps_0_ddr_mem_dqs_n   : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
		hps_0_ddr_mem_odt     : out   std_logic;                                        -- mem_odt
		hps_0_ddr_mem_dm      : out   std_logic_vector(3 downto 0);                     -- mem_dm
		hps_0_ddr_oct_rzqin   : in    std_logic                     := 'X';             -- oct_rzqin
		LEDh2f					 : out 	std_logic_vector(9 downto 0);
		butons					 : in    std_logic_vector(9 downto 0)  := (others => 'X') -- conduit
);
end OnChipInterface;

architecture main of OnChipInterface is
	component HPS_OnChip is
        port (
            clk_clk              : in    std_logic                     := 'X';             -- clk
            memory_mem_a         : out   std_logic_vector(14 downto 0);                    -- mem_a
            memory_mem_ba        : out   std_logic_vector(2 downto 0);                     -- mem_ba
            memory_mem_ck        : out   std_logic;                                        -- mem_ck
            memory_mem_ck_n      : out   std_logic;                                        -- mem_ck_n
            memory_mem_cke       : out   std_logic;                                        -- mem_cke
            memory_mem_cs_n      : out   std_logic;                                        -- mem_cs_n
            memory_mem_ras_n     : out   std_logic;                                        -- mem_ras_n
            memory_mem_cas_n     : out   std_logic;                                        -- mem_cas_n
            memory_mem_we_n      : out   std_logic;                                        -- mem_we_n
            memory_mem_reset_n   : out   std_logic;                                        -- mem_reset_n
            memory_mem_dq        : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            memory_mem_dqs       : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            memory_mem_dqs_n     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            memory_mem_odt       : out   std_logic;                                        -- mem_odt
            memory_mem_dm        : out   std_logic_vector(3 downto 0);                     -- mem_dm
            memory_oct_rzqin     : in    std_logic                     := 'X';             -- oct_rzqin
            leds_external_export : out   std_logic_vector(9 downto 0);                      -- export
				butons_external_conduit : in    std_logic_vector(9 downto 0)  := (others => 'X') -- conduit
        );
    end component HPS_OnChip;
begin
	u0 : component HPS_OnChip
        port map (
            clk_clk              => CLK_50,              --           clk.clk
            memory_mem_a         => hps_0_ddr_mem_a,         --        memory.mem_a
            memory_mem_ba        => hps_0_ddr_mem_ba,        --              .mem_ba
            memory_mem_ck        => hps_0_ddr_mem_ck,        --              .mem_ck
            memory_mem_ck_n      => hps_0_ddr_mem_ck_n,      --              .mem_ck_n
            memory_mem_cke       => hps_0_ddr_mem_cke,       --              .mem_cke
            memory_mem_cs_n      => hps_0_ddr_mem_cs_n,      --              .mem_cs_n
            memory_mem_ras_n     => hps_0_ddr_mem_ras_n,     --              .mem_ras_n
            memory_mem_cas_n     => hps_0_ddr_mem_cas_n,     --              .mem_cas_n
            memory_mem_we_n      => hps_0_ddr_mem_we_n,      --              .mem_we_n
            memory_mem_reset_n   => hps_0_ddr_mem_reset_n,   --              .mem_reset_n
            memory_mem_dq        => hps_0_ddr_mem_dq,        --              .mem_dq
            memory_mem_dqs       => hps_0_ddr_mem_dqs,       --              .mem_dqs
            memory_mem_dqs_n     => hps_0_ddr_mem_dqs_n,     --              .mem_dqs_n
            memory_mem_odt       => hps_0_ddr_mem_odt,       --              .mem_odt
            memory_mem_dm        => hps_0_ddr_mem_dm,        --              .mem_dm
            memory_oct_rzqin     => hps_0_ddr_oct_rzqin,     --              .oct_rzqin
            leds_external_export => LEDh2f,					    -- leds_external.export
				butons_external_conduit => butons
        );
end;

