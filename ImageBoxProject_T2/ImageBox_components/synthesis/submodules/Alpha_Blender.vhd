
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Alpha_Blender is
	port (
		clock_clk          : in  std_logic                     := '0';             --  clock.clk
		reset_reset        : in  std_logic                     := '0';             --  reset.reset
		avm_m0_address     : out std_logic_vector(16 downto 0);                     -- avm_m0.address
		avm_m0_read        : out std_logic;                                        --       .read
		avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
		avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
		avm_m0_write       : out std_logic;                                        --       .write
		avm_m0_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
		avm_m1_address     : out std_logic_vector(16 downto 0);                     -- avm_m1.address
		avm_m1_read        : out std_logic;                                        --       .read
		avm_m1_waitrequest : in  std_logic                     := '0';             --       .waitrequest
		avm_m1_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
		avm_m1_write       : out std_logic;                                        --       .write
		avm_m1_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
		avm_m2_address     : out std_logic_vector(16 downto 0);                     -- avm_m2.address
		avm_m2_read        : out std_logic;                                        --       .read
		avm_m2_waitrequest : in  std_logic                     := '0';             --       .waitrequest
		avm_m2_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
		avm_m2_write       : out std_logic;                                        --       .write
		avm_m2_writedata   : out std_logic_vector(31 downto 0);                     --       .writedata
		avs_s0_chipselect  	  : in  std_logic;
		avs_s0_read            : in  std_logic;          										 --       .read
		avs_s0_readdata        : out std_logic_vector(15 downto 0);                 	 --       .readdata
		avs_s0_write           : in  std_logic;            									 --       .write
		avs_s0_writedata       : in  std_logic_vector(15 downto 0)							 --       .writedata
	);
end entity Alpha_Blender;

architecture rtl of Alpha_Blender is
	signal alpha_factor : std_logic_vector(15 downto 0) := (others => '0');

	signal r1, r2 : std_logic_vector(4 downto 0);
	signal g1, g2 : std_logic_vector(5 downto 0);
	signal b1, b2 : std_logic_vector(4 downto 0);

	signal r_out : std_logic_vector(4 downto 0);
	signal g_out : std_logic_vector(5 downto 0);
	signal b_out : std_logic_vector(4 downto 0);
	
	signal data11 : std_logic_vector(31 downto 0);
	signal data12 : std_logic_vector(31 downto 0);
	
	type state_type is (s0, s1, s2, s3);
	signal state   : state_type := s0;
	
	
	
begin

	process (clock_clk)
	begin
	if(rising_edge(clock_clk)) then
		case state is
			when s0 =>
				if avm_m0_waitrequest = '0' then
					state <= s1;
				else
					state <= s0;
				end if;
			when s1 =>
					if avm_m0_waitrequest = '0' then
						state <= s2;
				else
					state <= s1;
				end if;
			when s2 =>
					if avm_m2_waitrequest = '0' then
						state <= s3;
				else
					state <= s2;
				end if;
			when s3 =>
				state <= s0;
		end case;
	end if;
	end process;

	process (state)
	begin
		case state is
			when s0 =>
				avm_m2_write <= '0';
				avm_m0_read <= '1';
				avm_m0_address <= "00000000000000001";
				data12 <= avm_m0_readdata;
			when s1 =>
				avm_m0_address <= "00000000000000000";
				data11 <= avm_m0_readdata;
			when s2 =>
			    avm_m0_read <= '0';
			    avm_m0_address <= "00000000000000001";
			    avm_m2_writedata <= data12;
			    avm_m2_write <= '1';
			when s3 =>
			    avm_m2_writedata <= data11;
			    avm_m2_address <= "00000000000000000";
				avm_m2_write <= '1';
		end case;
	end process;

end architecture rtl;


