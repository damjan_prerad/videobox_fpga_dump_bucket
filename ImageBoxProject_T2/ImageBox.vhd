

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ImageBox is
	port(
		xintput_clock_50_clk : in    std_logic                     := 'X';             -- clk
		xmemory_mem_a        : out   std_logic_vector(14 downto 0);                    -- mem_a
		xmemory_mem_ba       : out   std_logic_vector(2 downto 0);                     -- mem_ba
		xmemory_mem_ck       : out   std_logic;                                        -- mem_ck
		xmemory_mem_ck_n     : out   std_logic;                                        -- mem_ck_n
		xmemory_mem_cke      : out   std_logic;                                        -- mem_cke
		xmemory_mem_cs_n     : out   std_logic;                                        -- mem_cs_n
		xmemory_mem_ras_n    : out   std_logic;                                        -- mem_ras_n
		xmemory_mem_cas_n    : out   std_logic;                                        -- mem_cas_n
		xmemory_mem_we_n     : out   std_logic;                                        -- mem_we_n
		xmemory_mem_reset_n  : out   std_logic;                                        -- mem_reset_n
		xmemory_mem_dq       : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
		xmemory_mem_dqs      : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
		xmemory_mem_dqs_n    : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
		xmemory_mem_odt      : out   std_logic;                                        -- mem_odt
		xmemory_mem_dm       : out   std_logic_vector(3 downto 0);                     -- mem_dm
		xmemory_oct_rzqin    : in    std_logic                     := 'X';             -- oct_rzqin
		xsdram_bus_addr      : out   std_logic_vector(12 downto 0);                    -- addr
		xsdram_bus_ba        : out   std_logic_vector(1 downto 0);                     -- ba
		xsdram_bus_cas_n     : out   std_logic;                                        -- cas_n
		xsdram_bus_cke       : out   std_logic;                                        -- cke
		xsdram_bus_cs_n      : out   std_logic;                                        -- cs_n
		xsdram_bus_dq        : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
		xsdram_bus_dqm       : out   std_logic_vector(1 downto 0);                     -- dqm
		xsdram_bus_ras_n     : out   std_logic;                                        -- ras_n
		xsdram_bus_we_n      : out   std_logic;                                        -- we_n
		xvga_interface_CLK   : out   std_logic;                                        -- CLK
		xvga_interface_HS    : out   std_logic;                                        -- HS
		xvga_interface_VS    : out   std_logic;                                        -- VS
		xvga_interface_BLANK : out   std_logic;                                        -- BLANK
		xvga_interface_SYNC  : out   std_logic;                                        -- SYNC
		xvga_interface_R     : out   std_logic_vector(7 downto 0);                     -- R
		xvga_interface_G     : out   std_logic_vector(7 downto 0);                     -- G
		xvga_interface_B     : out   std_logic_vector(7 downto 0);                     -- B
		xled_bus_export      : out   std_logic_vector(9 downto 0);                      -- export	
		xsdram_clock_clk     : out   std_logic                                         -- clk
	);
end ImageBox;


architecture main of ImageBox is
    component ImageBox_components is
        port (
            intput_clock_50_clk : in    std_logic                     := 'X';             -- clk
            memory_mem_a        : out   std_logic_vector(14 downto 0);                    -- mem_a
            memory_mem_ba       : out   std_logic_vector(2 downto 0);                     -- mem_ba
            memory_mem_ck       : out   std_logic;                                        -- mem_ck
            memory_mem_ck_n     : out   std_logic;                                        -- mem_ck_n
            memory_mem_cke      : out   std_logic;                                        -- mem_cke
            memory_mem_cs_n     : out   std_logic;                                        -- mem_cs_n
            memory_mem_ras_n    : out   std_logic;                                        -- mem_ras_n
            memory_mem_cas_n    : out   std_logic;                                        -- mem_cas_n
            memory_mem_we_n     : out   std_logic;                                        -- mem_we_n
            memory_mem_reset_n  : out   std_logic;                                        -- mem_reset_n
            memory_mem_dq       : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            memory_mem_dqs      : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            memory_mem_dqs_n    : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            memory_mem_odt      : out   std_logic;                                        -- mem_odt
            memory_mem_dm       : out   std_logic_vector(3 downto 0);                     -- mem_dm
            memory_oct_rzqin    : in    std_logic                     := 'X';             -- oct_rzqin
            sdram_bus_addr      : out   std_logic_vector(12 downto 0);                    -- addr
            sdram_bus_ba        : out   std_logic_vector(1 downto 0);                     -- ba
            sdram_bus_cas_n     : out   std_logic;                                        -- cas_n
            sdram_bus_cke       : out   std_logic;                                        -- cke
            sdram_bus_cs_n      : out   std_logic;                                        -- cs_n
            sdram_bus_dq        : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
            sdram_bus_dqm       : out   std_logic_vector(1 downto 0);                     -- dqm
            sdram_bus_ras_n     : out   std_logic;                                        -- ras_n
            sdram_bus_we_n      : out   std_logic;                                        -- we_n
            vga_interface_CLK   : out   std_logic;                                        -- CLK
            vga_interface_HS    : out   std_logic;                                        -- HS
            vga_interface_VS    : out   std_logic;                                        -- VS
            vga_interface_BLANK : out   std_logic;                                        -- BLANK
            vga_interface_SYNC  : out   std_logic;                                        -- SYNC
            vga_interface_R     : out   std_logic_vector(7 downto 0);                     -- R
            vga_interface_G     : out   std_logic_vector(7 downto 0);                     -- G
            vga_interface_B     : out   std_logic_vector(7 downto 0);                     -- B
            led_bus_export      : out   std_logic_vector(9 downto 0);                     -- export
				sdram_clock_clk     : out   std_logic                                         -- clk
        );
    end component ImageBox_components;

begin
	 
    u0 : component ImageBox_components
        port map (
            intput_clock_50_clk => xintput_clock_50_clk, -- intput_clock_50.clk
            memory_mem_a        => xmemory_mem_a,        --          memory.mem_a
            memory_mem_ba       => xmemory_mem_ba,       --                .mem_ba
            memory_mem_ck       => xmemory_mem_ck,       --                .mem_ck
            memory_mem_ck_n     => xmemory_mem_ck_n,     --                .mem_ck_n
            memory_mem_cke      => xmemory_mem_cke,      --                .mem_cke
            memory_mem_cs_n     => xmemory_mem_cs_n,     --                .mem_cs_n
            memory_mem_ras_n    => xmemory_mem_ras_n,    --                .mem_ras_n
            memory_mem_cas_n    => xmemory_mem_cas_n,    --                .mem_cas_n
            memory_mem_we_n     => xmemory_mem_we_n,     --                .mem_we_n
            memory_mem_reset_n  => xmemory_mem_reset_n,  --                .mem_reset_n
            memory_mem_dq       => xmemory_mem_dq,       --                .mem_dq
            memory_mem_dqs      => xmemory_mem_dqs,      --                .mem_dqs
            memory_mem_dqs_n    => xmemory_mem_dqs_n,    --                .mem_dqs_n
            memory_mem_odt      => xmemory_mem_odt,      --                .mem_odt
            memory_mem_dm       => xmemory_mem_dm,       --                .mem_dm
            memory_oct_rzqin    => xmemory_oct_rzqin,    --                .oct_rzqin
            sdram_bus_addr      => xsdram_bus_addr,      --       sdram_bus.addr
            sdram_bus_ba        => xsdram_bus_ba,        --                .ba
            sdram_bus_cas_n     => xsdram_bus_cas_n,     --                .cas_n
            sdram_bus_cke       => xsdram_bus_cke,       --                .cke
            sdram_bus_cs_n      => xsdram_bus_cs_n,      --                .cs_n
            sdram_bus_dq        => xsdram_bus_dq,        --                .dq
            sdram_bus_dqm       => xsdram_bus_dqm,       --                .dqm
            sdram_bus_ras_n     => xsdram_bus_ras_n,     --                .ras_n
            sdram_bus_we_n      => xsdram_bus_we_n,      --                .we_n
            vga_interface_CLK   => xvga_interface_CLK,   --   vga_interface.CLK
            vga_interface_HS    => xvga_interface_HS,    --                .HS
            vga_interface_VS    => xvga_interface_VS,    --                .VS
            vga_interface_BLANK => xvga_interface_BLANK, --                .BLANK
            vga_interface_SYNC  => xvga_interface_SYNC,  --                .SYNC
            vga_interface_R     => xvga_interface_R,     --                .R
            vga_interface_G     => xvga_interface_G,     --                .G
            vga_interface_B     => xvga_interface_B,     --                .B
            led_bus_export      => xled_bus_export,      --         led_bus.export
				sdram_clock_clk	  => xsdram_clock_clk
        );

end architecture main;