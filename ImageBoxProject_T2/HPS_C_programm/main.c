#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"
#include <math.h>

//#include "soc_cv_av/socal/socal.h"
//#include "soc_cv_av/socal/hps.h"
//#include "soc_cv_av/socal/alt_gpio.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"

#include <pthread.h>

#include "ImageBox_memmory_offsets.h"
#include "earth.h"
#include "train.h"
#include "test2.h"
#include "test3.h"
#include "test4.h"
#include "test5.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

#define ALT_AXI_FPGASLVS_OFST (0xC0000000) // axi_master
#define HW_FPGA_AXI_SPAN (0x40000000) // Bridge span 1GB
#define HW_FPGA_AXI_MASK ( HW_FPGA_AXI_SPAN - 1 )

#define IMG_OFFSET 0x01000000

#define BACK_BUFFER_OFFSET 4
#define BUFFER_OFFSET 0

#define CONTROL_OFFSET 12

uint8_t swap = 0;
uint32_t addr_off = 0;

pthread_t user_interface_thread;

uint16_t buffer1[480][640] = { 0 };//{0xff, 0xff, 0xff}, {0x00, 0x00, 0xff}, {0x00, 0xff, 0x00}, {0xff, 0x00, 0x00} };

void *addr_pixel_image1;
void* 	addr_h2f_led;
void *addr_alpha_blender;

void *addr_dma_controller;

volatile char buffer_switch = 'a';
volatile char img_selector = 0;
volatile char stop = 0;

int number = 0;
int division = 4;

char write_always = 0;

void *user_interface_function(void* not_used)
{
	while(buffer_switch != 'q')
	{
		printf("Unesi znak.\n");
		fflush(stdin);
		fflush(stdout);
		scanf(" %c", &buffer_switch);
		
		if(buffer_switch == 'a')
		{
			printf("Unesi vrijednost alfa.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &number);
			*((uint32_t*)addr_h2f_led) = number;
			//*((uint16_t*)addr_alpha_blender) = number;
		}
		if(buffer_switch == 'b')
		{
			printf("Unesi img selector.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &img_selector);
		}
		if(buffer_switch == 'p')
		{
			stop = 1;
		}
		if(buffer_switch == 'o')
		{
			printf("Unesi vrijednost dijeljena.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &division);
		}
		if(buffer_switch == 'i')
		{
			printf("Pisi uvijek 1, pisi jednom 0.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &write_always);
			printf("Write_always je : %d", write_always); 
		}
	}
}

void drawCircle(int x, int y, int r, uint8_t R, uint8_t G, uint8_t B, uint16_t buffer[480][640])
{
	int i, j;
	for(i = 0; i < 480; i++)
	{
		for(j = 0; j < 640; j++)
		{
			if( (i - y) * (i - y) + (j - x) * (j - x) < r * r) 
			{
				buffer[i][j] |= ((R & 0x1f) << 11);
				buffer[i][j] |= ((G & 0x3f) << 6);
				buffer[i][j] |= (B & 0x1f);
			}
		}
	}
}

void drawBox(int x1, int y1, int x2, int y2, uint8_t R, uint8_t G, uint8_t B, uint16_t buffer[480][640])
{
	int i, j;
	for(i = y1; i < y2; i++)
	{
		for(j = x1; j < x2; j++)
		{
			buffer[i][j] |= ((R & 0x1f) << 11);
			buffer[i][j] |= ((G & 0x3f) << 6);
			buffer[i][j] |= (B & 0x1f); 
		}
	}
}

void clearScreen(uint16_t buffer[480][640])
{
	int i, j;
	for(i = 0; i < 480; i++)
	{
		for(j = 0; j < 640; j++)
		{
			buffer[i][j] = 0;
		}
	}
}

#define PI 3.14159265358

int x = 320, y = 240;
double t = 0;

void generateRotatingCircle(void)
{
	x = 320 + 50 * cos(2 * PI * t);
	y = 240 + 50 * sin(2 * PI * t);
	t += 0.01;
	
	
	clearScreen(buffer1);
	drawCircle(x, y, 10, 255, 0, 0, buffer1);
	drawCircle(320, 240, 10, 0, 0, 255, buffer1);
	
	drawCircle(160, 50, 10, 255, 0, 0, buffer1);
	drawCircle(320, 50, 10, 0, 255, 0, buffer1);
	drawCircle(480, 50, 10, 0, 0, 255, buffer1);
	
	drawBox(317, 220, 323, 260, 255, 255, 255, buffer1);
	drawBox(300, 237, 340, 243, 0, 50, 128, buffer1);
}

void generateGraph(int x, int y, uint8_t R, uint8_t G, uint8_t B)
{
	int i = 0;
	for( ;i < 255; i++)
	{
		drawBox(112 + i, 150 - i * i,112 + i + 1, 150, 255, 0, i, buffer1);
		drawBox(112 + i, 250 - i,112 + i + 1, 250, i, 255, 0, buffer1);
		drawBox(112 + i, 350 - i, 350, 0, i, 255, 0, buffer1);
	}
}

int main(int argc, char* argv[])
{
	void*	virtual_base;
	void* 	axi_virtual_base;
	
	int 	fd;
	void*	addr_lwh2f_led;
	
	
	(void)argc;
	(void)argv;

	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span
	if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		printf("ERROR: could not open \"/dev/mem\"...\n");
		
		return 1;
	}

	//virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, HW_REGS_BASE);
	axi_virtual_base = mmap( NULL, HW_FPGA_AXI_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, ALT_AXI_FPGASLVS_OFST );
	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );
	
	
	if (axi_virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}

	if (virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}
	
	//addr_h2f_led = axi_virtual_base + ((unsigned long)(LEDH2F_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	addr_h2f_led = axi_virtual_base + ((unsigned long)(LED_CONTROL_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	addr_pixel_image1 = axi_virtual_base + ((unsigned long)(SDRAM_CONTROLLER_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	//addr_led = axi_virtual_base + ((unsigned long)(LEDH2F_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	//addr_alpha_blender = axi_virtual_base + ((unsigned long)(ALPHA_BLENDER_STREAM_V2_0_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	int i, j;
	
	if(pthread_create(&user_interface_thread, NULL, user_interface_function, NULL)) {
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}
		
	//drawCircle(120, 240, 20, 0, 255, 0);
	//drawCircle(420, 240, 35, 0, 0, 255);
	
	uint32_t buff_of;
	while(buffer_switch != 'q')
	{	
		generateRotatingCircle();
		//generateGraph(112, 150, 255, 0, 0);
		
		uint32_t adr = 0;
		for(i = 0; i < 480; i++)
		{
			for(j = 0; j < 640; j++)
			{
				if(img_selector == 1)
					*((uint16_t*)addr_pixel_image1 + j + i * 640) = earth[i][j];
				if(img_selector == 2)
					*((uint16_t*)addr_pixel_image1 + j + i * 640) = train[i][j];
				if(img_selector == 3)
					*((uint16_t*)addr_pixel_image1 + j + i * 640) = test2[i][j];
				if(img_selector == 4)
					*((uint16_t*)addr_pixel_image1 + j + i * 640) = test3[i][j];
				if(img_selector == 5)
					*((uint16_t*)addr_pixel_image1 + j + i * 640) = test4[i][j];
				
			}
		}
		usleep(10 * 1000);
	}

	// Clean up our memory mapping and exit
	if (munmap(axi_virtual_base, HW_FPGA_AXI_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}

	if (munmap(virtual_base, HW_REGS_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}
	
	close(fd);

	return 0;
}
