#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"

//#include "soc_cv_av/socal/socal.h"
//#include "soc_cv_av/socal/hps.h"
//#include "soc_cv_av/socal/alt_gpio.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"

#include "HPS_PIO.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

#define ALT_AXI_FPGASLVS_OFST (0xC0000000) // axi_master
#define HW_FPGA_AXI_SPAN (0x40000000) // Bridge span 1GB
#define HW_FPGA_AXI_MASK ( HW_FPGA_AXI_SPAN - 1 )

#define OFST 10

int main(int argc, char* argv[])
{
	void*	virtual_base;
	void* 	axi_virtual_base;
	
	int 	fd;
	void*	addr_lwh2f_led;
	void* 	addr_h2f_led;
	
	(void)argc;
	(void)argv;

	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span
	if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		printf("ERROR: could not open \"/dev/mem\"...\n");
		
		return 1;
	}

	//virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, HW_REGS_BASE);
	axi_virtual_base = mmap( NULL, HW_FPGA_AXI_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, ALT_AXI_FPGASLVS_OFST );
	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );
	
	
	if (axi_virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}

	if (virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}
	
	addr_h2f_led = axi_virtual_base + ((unsigned long)(LEDH2F_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));
	addr_lwh2f_led = virtual_base + ((unsigned long)(ALT_LWFPGASLVS_OFST + LEDLWH2F_BASE) & (unsigned long)(HW_REGS_MASK));
	
	int i, j;
	
	while (1) {
		int a;
		scanf("%d", &a);
		alt_write_word(addr_h2f_led + 0, a);
		alt_write_word(addr_lwh2f_led + 0, a);
		//*(uint32_t *)addr_h2f_led = a;
		//*(uint32_t *)addr_lwh2f_led = a;
		
	}

	// Clean up our memory mapping and exit
	if (munmap(axi_virtual_base, HW_FPGA_AXI_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}

	if (munmap(virtual_base, HW_REGS_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}
	
	close(fd);

	return 0;
}
