library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fsm_tb is
end entity fsm_tb;

architecture rtl of fsm_tb is
	component ab_fsm is
	port (
		clock_clk          : in  std_logic                     := '0';
		reset_reset        : in  std_logic                     := '0';
		avm_m0_waitrequest : in  std_logic                     := '0';
		avm_m2_waitrequest : in  std_logic                     := '0';
		state_out : out integer
	);
	end component;
	
	signal clk : std_logic := '0';
	signal reset : std_logic := '0';
	signal state : integer := 0;
	
	signal wr0 : std_logic := '1';
	signal wr1 : std_logic := '1';
	
	constant clk_period : time := 10 ns;
	
begin
	fsm_inst : ab_fsm port map(clk, reset, wr0, wr1, state);
	
	clk_gen: process is
	begin
        clk <= '1';
        wait for clk_period / 2;
        clk <= '0';
        wait for clk_period / 2;
	end process clk_gen;
	
	
	main : process is
	begin
		wait for clk_period;
		wr0 <= '0';
		
		wait for clk_period;
		wr1 <= '0';
		wr0 <= '1';
		
		wait for clk_period;
		wr0 <= '0';
		wr1 <= '1';
		
		wait for clk_period;
		wr0 <= '0';
		wr1 <= '0';
		
		wait for clk_period;
		wr0 <= '0';
		wr1 <= '0';
		
		-- etc
		
		wait;
		
	end process main;

end architecture rtl; -- of fsm_tb