library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ab_fsm is
	port (
		clock_clk          : in  std_logic                     := '0';
		reset_reset        : in  std_logic                     := '0';
		avm_m0_waitrequest : in  std_logic                     := '0';
		avm_m2_waitrequest : in  std_logic                     := '0';
		state_out : out integer -- used for testing - exporting to testbench
	);
end entity ab_fsm;

architecture rtl of ab_fsm is

type state_type is (s0, s1, s2, s3);
signal state   : state_type := s0;



begin
	process (clock_clk)
	begin
	if(rising_edge(clock_clk)) then
		case state is
			when s0 =>
				state_out <= 0;
				if avm_m0_waitrequest = '0' then
					state <= s1;
				else
					state <= s0;
				end if;
			when s1 =>
					state_out <= 1;
					if avm_m0_waitrequest = '0' then
						state <= s2;
				else
					state <= s1;
				end if;
			when s2 =>
					state_out <= 2;
					if avm_m2_waitrequest = '0' then
						state <= s3;
				else
					state <= s2;
				end if;
			when s3 =>
				state_out <= 3;
				state <= s0;
		end case;
	end if;
	end process;

end architecture rtl; -- of ab_fsm