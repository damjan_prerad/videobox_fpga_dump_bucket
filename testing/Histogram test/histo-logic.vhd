library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity histo_logic is 
	generic (
		data_width : integer := 32; -- Default size is 32 bits in onchip memory 
		addr_width : integer := 8
		);
	port (
		clk		: in std_logic;
		rst		: in std_logic; -- reset used in histogram 
		
		h_ready	: in std_logic; 	-- signal that indicates address is set (addr_a) and histogram is ready for increment 
		h_done	: out std_logic; 	-- signal that indicates increment is done
		
		addr_a	: in std_logic_vector(addr_width-1 downto 0); -- pixel value is set as address a in histogram entity 
		addr_b 	: in std_logic_vector(addr_width-1 downto 0); -- address for reading values 
		out_b		: out std_logic_vector(data_width-1 downto 0) -- port used for reading value
		
		);
end histo_logic;

architecture arch of histo_logic is 

	signal write_register: std_logic_vector(data_width-1 downto 0); -- Internal register mapped to RAM din_a, incremented value is forwareded here
	signal read_register : std_logic_vector(data_width-1 downto 0); -- Internal register mapped to RAM dout_a, current RAM value is read to this register
	
	signal increment 	: unsigned (data_width - 1 downto 0) := (0 =>'1', others =>'0'); -- vidjeti da li moze const ovdje
	
	signal RAM_we_a : std_logic; -- for testing only 
	signal RAM_we_b : std_logic := '0'; -- write will not be done by channel b 
	
	
	-- Build an enumerated type for the state machine
	type incrementing_state_type is (s0, s1, s2, s3) ;

	-- Register to hold the current state
	signal state : incrementing_state_type := s0;

begin
	Memory : entity work.RAM_new
	generic map (
		ADDR_WIDTH => addr_width, 
      DATA_WIDTH => data_width)
   port map (
					clk		=> clk,
					we_a 		=> RAM_we_a,-- internally set
					we_b 		=> RAM_we_b,-- internally set
					rst		=> rst,		
					addr_a 	=> addr_a,	-- set from outside
					addr_b 	=> addr_b,	-- set from outside 
					
					data_a 	=> write_register,	-- used for correct incrementing
					q_a		=> read_register, 	-- used for correct incrementing
					
					data_b 	=> (others => '0'),	-- never used
					q_b 		=> out_b 				-- read from outside
					);
					
					
process(clk)

	begin
		if rising_edge(clk) then 
		case state is
			when s0=>
				if h_ready = '1' and rst ='0' then -- When signalised that addr_a is stable, go to first state
					state <= s1;
				else
					state <= s0;
				end if;
			when s1=>
				if rst = '1' then
					state <= s0;
				else
					state <= s2;
				end if;
			when s2=>
				if rst = '1' then 
					state <= s0;
				elsif h_ready = '0' then -- There has to be change in h_ready signal, else it would loop and increment value multiple times
					state <= s3;
				end if;
			when s3=>
				if h_ready = '1' then 		-- If new data is ready then go into state 1
					state <= s1;
				elsif (rst = '1') then
					state <= s0;
				else 
					state <= s3;
				end if;
			end case;
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state,clk)
	begin
			case state is
				when s0=>
					RAM_we_a <= '0';
					RAM_we_b <= '0';
					h_done 	<= '0'; 
				when s1=>
					h_done 	<= '0'; 
					write_register <= std_logic_vector(unsigned(read_register) + increment);
					RAM_we_a <= '1';
				when s2=>
					h_done 	<= '1';
					RAM_we_a <= '0';
				when s3 =>
					null;
				end case;
	end process;
end arch;