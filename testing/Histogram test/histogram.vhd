-- Histogram with implemented mm masters 
-- NOTE: This version has waitrequest signals implemented, on recent tests waitrequest is not nessesery on writing at least.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity histogram is
    port (
-- Avalon memory mapped master for reading pixel value
			avm_m0_address     : out std_logic_vector(31 downto 0);                     -- avm_m0.address
			avm_m0_read        : out std_logic;                                        --       .read
			avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m0_write       : out std_logic;                                        --       .write
			avm_m0_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
-- Clock and reset global
			clock_clk          : in  std_logic                     := '0';             --  clock.clk
			reset_reset        : in  std_logic                     := '0';             --  reset.reset
-- Avalon mm master for writting histogram 
			avm_m1_address     : out std_logic_vector(31 downto 0);                     -- avm_m1.address
			avm_m1_read        : out std_logic;                                        --       .read
			avm_m1_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m1_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m1_write       : out std_logic;                                        --       .write
			avm_m1_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata	
		
			next_read			: out std_logic;
			stanje				: out	integer
);
end entity histogram;

architecture rtl of histogram is
	
	-- Build an enumerated type for the state machine
	type read_states is (p0, s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17);

	-- Register to hold the current state
	signal state : read_states := p0;  -- states from memory mapped master 1 for reading 

	signal SOP, EOP, FIP 	:std_logic := '0'; -- SOP - start of packet, EOP - end of packet, FIP - Frame in progress 
	signal data1		 		:std_logic_vector(31 downto 0) := (others => '0'); -- for reading data values/writing

	signal h_r_done, h_g_done, h_b_done : std_logic := '0'; -- Control signals from respective histograms
	signal h_t_done			: std_logic := '0'; -- Control signal that indicates histogram writingto on-chip memory is done
	signal pixel_ready		: std_logic := '0'; -- Control signal that indicates that pixel is ready to be send to histo_logic
	signal reset_histograms : std_logic := '0'; -- Value for reseting all 3 histograms ( could be variable)
	
	signal red_offset			: integer := 0;
	signal green_offset		: integer := 1280;
	signal blue_offset		: integer := 2560;

	signal red_address		: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal red_value			: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal green_address	  	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal green_value 		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal blue_address	  	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal blue_value 		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	constant addr_width 		: integer 	:= 32; --for generic map if it is nessesery 
	constant data_width 		: integer 	:= 8;

begin
	
	histo_red : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_r_done,		-- Signalises that one increment is done
			
			addr_a	=> red_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> red_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b		=> red_value
			);
	
	histo_green : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_b_done,		-- Signalises that one increment is done
			
			addr_a	=> blue_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> blue_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b		=> blue_value
			);
	
	histo_blue : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_g_done,		-- Signalises that one increment is done
			
			addr_a	=> green_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> green_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b		=> green_value
			);
	
	process (clock_clk)
	begin
		if(rising_edge(clock_clk)) then 
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when p0 => -- In simulations, state machine just runs through state s0 so this is additional state to make sure state s0 is reached 
					if avm_m0_waitrequest = '0' then 
						state <= s0;
					else 
						state <= p0;
					end if;
				when s0 =>		-- State s0 is used for reseting memory and reseting h_t_done signal
					if avm_m0_waitrequest = '0' then 
						state <= s1;
					else 
						state <= s0;
					end if;
				when s1 =>		-- Set m0_read, set reset_histograms to 0, set pixel_ready to 0 and set address to 0x00000001
					if avm_m0_waitrequest = '0' then
						state <= s2;
					else
						state <= s1;
					end if;
				when s2 =>		-- Set SOP and EOP signals, set address to 0x00000000 
					if avm_m0_waitrequest = '0' then 
						state <= s3;
					else 
						state <= s2;
					end if;
				when s3 =>		-- Read 32 bits of data, pixels from both images and pop it from fifo
					if avm_m0_waitrequest = '0' then
						if SOP = '0' and FIP = '0' then 	-- If there is no SOP and FIP flags, go into state s1
							state <= s1;
						else		
							state <= s4;		-- go into histogram incrementing state
						end if;
					else 
						state <= s3;
					end if;
				when s4 =>						-- Set FIP flag
					if avm_m0_waitrequest = '0' then 
						state <= s5;
					else 
						state <= s4;
					end if;
				when s5 =>		-- Increment value of histograms
					if avm_m0_waitrequest = '0' and h_r_done = '1' and h_b_done = '1' and h_g_done = '1' and EOP = '0' then 
						state <= s1;
					elsif	h_r_done = '1' and h_b_done = '1' and h_g_done = '1' and EOP = '1' then	-- No need for avm_m0_waitrequest because no avm_m0 signals are set in stage s6 or s5
						state <= s6;
					else 
						state <= s5;
					end if;
				when s6 =>		-- Set address to 0 and start looping 
					if avm_m1_waitrequest = '0' then 
						state <= s7;
					else 
						state <= s6;
					end if;
				when s7 =>		
					if avm_m1_waitrequest = '0' and h_t_done = '0' then 
						state <= s8; 
					elsif h_t_done = '1' then
						state <= s10;
					else
						state <= s7;
					end if;
				when s8 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s9;
					else
						state <= s8;
					end if;
				when s9 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s7;
					else
						state <= s9;
					end if;
				when s10 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s11;
					else
						state <= s9;
					end if;
				when s11 =>		
					if avm_m1_waitrequest = '0' and h_t_done = '0' then 
						state <= s12; 
					elsif h_t_done = '1' then
						state <= s14;
					else
						state <= s11;
					end if;
				when s12 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s13;
					else
						state <= s12;
					end if;
				when s13 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s11;
					else
						state <= s13;
					end if;
				when s14 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s15;
					else
						state <= s14;
					end if;
				when s15 =>		
					if avm_m1_waitrequest = '0' and h_t_done = '0' then 
						state <= s16; 
					elsif h_t_done = '1' then
						state <= s0;
					else
						state <= s15;
					end if;
				when s16 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s17;
					else
						state <= s16;
					end if;
				when s17 =>		
					if avm_m1_waitrequest =	'0' then
						state <= s15;
					else
						state <= s17;
					end if;
			end case;
		end if;	
	end process;

	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state)
		
	variable address 			: integer	:= 0;		
	variable read_addr			: integer 	:= 0;
	begin
			case state is
				when p0 =>
					reset_histograms 	<= '1';
					h_t_done				<= '0';
					avm_m0_read			<= '0';
					avm_m0_write		<= '0';
					avm_m0_address 	<= (2 => '1', others => '0');	-- Or base value 
					avm_m1_read			<= '0';
					avm_m1_write		<= '0';
					avm_m1_address 	<= (others => '0');	-- Or base value
					stanje <= 100;
				when s0 =>
					reset_histograms 	<= '1';	-- Reset histograms
					h_t_done				<= '0';
					pixel_ready			<= '0';
					avm_m0_read			<= '0';
					avm_m0_write		<= '0';
					avm_m0_address 	<= (0 => '1', others => '0');	-- Or base value 
					avm_m1_read			<= '0';
					avm_m1_write		<= '0';
					avm_m1_address 	<= (others => '0');	-- Or base value
					stanje <= 0;
				when s1 =>		--	Set everything up for new cycle of reading 
					reset_histograms 	<= '0';
					pixel_ready			<= '0';
					avm_m0_read			<= '1';
					avm_m0_write		<= '0';
					avm_m0_address 	<= (0 => '1', others => '0');	-- Or base value 
					avm_m1_read			<= '0';
					avm_m1_write		<= '0';
					avm_m1_address 	<= (others => '0');	-- Or base value
					stanje <= 1;
				when s2 =>		-- Read SOP and EOP from base+offset value and change address to base (0) address
					SOP 	<= avm_m0_readdata(0);								-- According to datasheet
					EOP 	<= avm_m0_readdata(1);
					avm_m0_address <= (others => '0');
					stanje <= 2;
				when s3 =>		--	Read pixels, pop value from FIFO (when value from base is read it pops it out) and set read to 0
					data1 <= avm_m0_readdata;
					avm_m0_read <= '0';
					stanje <= 3;
				when s4 =>		-- Set FIP parameter 
					if	EOP = '1' then
						FIP <= '0';
					elsif	SOP = '1' then
						FIP <= '1';
					end if;
					stanje <= 4;
				when s5 =>		-- Set read addresses 
					stanje <= 5;
					red_address					  	<= data1(23 downto 16); 				
					green_address					<= data1(15 downto 8);
					blue_address					<= data1(7 downto 0);
					pixel_ready						<= '1'; -- Ovo srediti
					if EOP = '1' then
						next_read 	<= '0';
					else 
						next_read 	<= '1';
					end if;
				when s6 =>
					stanje <= 6;
					address 		:= 0;
					read_addr		:= 0;
					h_t_done		<= '0';			-- Typical do-while loop
				when s7 =>
					stanje <= 7;
					if h_t_done = '0' then	-- Make sure address is correct  
						red_address			<= std_logic_vector(to_unsigned(read_addr, data_width));	-- Convert integer to std_logic_vector 
					end if;
				when s8 =>
					stanje <= 8;
					avm_m1_address		<= std_logic_vector(to_unsigned((address + red_offset), addr_width));		-- This conversion could make trouble
					avm_m1_writedata	<= red_value;
					avm_m1_write		<= '1';
				when s9 =>
					stanje <= 9;
					avm_m1_write		<= '0';
					if read_addr > 255 then
						h_t_done			<= '1';
					else
						read_addr			:= read_addr + 1;
						address 			:= address + 4;
						h_t_done			<= '0';
					end if;
				when s10 =>
					stanje <= 10;
					read_addr				:= 0;
					address 				:= 0;
					h_t_done 			<= '0';
					avm_m1_write		<= '0';
				when s11 =>
					stanje <= 11;
					if h_t_done = '0' then 
						green_address		<= std_logic_vector(to_unsigned(read_addr, data_width));	-- Convert integer to std_logic_vector
					end if;
				when s12 =>
					stanje <= 12;
					avm_m1_address		<= std_logic_vector(to_unsigned((address + green_offset), addr_width));		-- This conversion could make trouble 
					avm_m1_writedata	<= green_value;
					avm_m1_write		<= '1';
				when s13 =>
					stanje <= 13;
					avm_m1_write		<= '0';
					if read_addr > 255 then 
						h_t_done 		<= '1';
					else 
						read_addr			:= read_addr + 1;
						address 			:= address + 4;
						h_t_done			<= '0';
					end if;
				when s14 =>
					stanje <= 14;
					read_addr				:= 0;
					address 				:= 0;
					h_t_done				<= '0';
				when s15 =>
					stanje <= 15;
					if h_t_done = '0' then 
						blue_address		<= std_logic_vector(to_unsigned(read_addr, data_width));	-- Convert integer to std_logic_vector
					end if;
				when s16 =>
					stanje <= 16;
					avm_m1_address		<= std_logic_vector(to_unsigned((address + blue_offset), addr_width));		-- This conversion could make trouble 
					avm_m1_writedata	<= blue_value;
					avm_m1_write		<= '1';
				when s17 =>
					stanje <= 17;
					avm_m1_write		<= '0';
					if read_addr > 255 then 
						h_t_done 		<= '1';
						next_read		<= '1';
					else 
						read_addr			:= read_addr + 1;
						address 			:= address + 4;
						h_t_done			<= '0';
					end if;
			end case;
	end process;

end architecture rtl; 