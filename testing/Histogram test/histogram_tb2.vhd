-- This is testbench for histogram, it uses one input csv file marked as input_buf and stores histograms in 
-- output csv file output_buf

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity histogram_tb2 is
end entity histogram_tb2;


architecture arch of histogram_tb2 is

-- Sigals for mapping 		
-- Avalon memory mapped master for reading pixel value
	signal	avm_m0_address     	: std_logic_vector(31 downto 0);                    -- avm_m0.address
	signal	avm_m0_read        	: std_logic;                                        --       .read
	signal	avm_m0_waitrequest 	: std_logic                     := '0';             --       .waitrequest
	signal	avm_m0_readdata    	: std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
	signal	avm_m0_write       	: std_logic;                                        --       .write
	signal	avm_m0_writedata   	: std_logic_vector(31 downto 0);                    --       .writedata
-- Clock and reset global
	signal	clock_clk          	: std_logic                     := '0';             --  clock.clk
	signal	reset_reset        	: std_logic                     := '0';             --  reset.reset
-- Avalon mm master for writting histogram 
	signal	avm_m1_address     	: std_logic_vector(31 downto 0);                    -- avm_m1.address
	signal	avm_m1_read        	: std_logic ;                                        --       .read
	signal	avm_m1_waitrequest 	: std_logic                     := '0';             --       .waitrequest
	signal	avm_m1_readdata    	: std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
	signal	avm_m1_write       	: std_logic;                                        --       .write
	signal	avm_m1_writedata   	: std_logic_vector(31 downto 0);                    --       .writedata
	
-- Signals required for testing 	
	file input_buf 	: text;
	file output_buf	: text;
	
	constant period 		: time		:= 10 ns;	-- This is equal to 100 MHz clock
	signal next_read 		: std_logic; -- Required for test only 
	signal end_simulation	: std_logic :='0' ; -- To end simulation 
	signal stanje 			: integer;				-- for testing
	
	-- FSM for testing
	type test_states is (s0, s1, s2, s3, s4);
	signal state : test_states := s0;   
		
begin
	
	histogram : entity work.histogram
   port map (
-- Avalon memory mapped master for reading pixel value
			avm_m0_address    =>	avm_m0_address,                     -- avm_m0.address
			avm_m0_read       =>	avm_m0_read,                        --       .read
			avm_m0_waitrequest=>	avm_m0_waitrequest,						--       .waitrequest
			avm_m0_readdata   =>	avm_m0_readdata, 							--       .readdata
			avm_m0_write      =>	avm_m0_write,                       --       .write
			avm_m0_writedata  =>	avm_m0_writedata,                   --       .writedata
-- Clock and reset global
			clock_clk         =>	clock_clk,					            --  clock.clk
			reset_reset       =>	reset_reset,           				  	--  reset.reset
-- Avalon mm master for writting histogram 
			avm_m1_address    =>	avm_m1_address,                     -- avm_m0.address
			avm_m1_read       =>	avm_m1_read,                        --       .read
			avm_m1_waitrequest=>	avm_m1_waitrequest,						--       .waitrequest
			avm_m1_readdata   =>	avm_m1_readdata, 							--       .readdata
			avm_m1_write      =>	avm_m1_write,                       --       .write
			avm_m1_writedata  =>	avm_m1_writedata,                   --       .writedata
			
			next_read 			=>	next_read,
			stanje				=>	stanje
			);
		
	file_open(input_buf, "D:\FPGA\Tests\Histogram test\lena.csv",  read_mode); 			-- Change path to match your computer 
	file_open(output_buf, "D:\FPGA\Tests\Histogram test\lena_out.csv",  write_mode); 

	
	clock: process
	begin
		clock_clk <= '0';
		wait for period/2;
		clock_clk <= '1';
		wait for period/2;
		if end_simulation = '1' then -- Close files and end simulation
			file_close(input_buf);
			file_close(output_buf);
			wait;
		end if;
	end process;
	
	histogram_state_change: process(clock_clk)
	begin
	if rising_edge(clock_clk) then
		case state is
			when s0 => -- Read first data word and forward it to avm_m0_readdata and avm_m0_waitrequest
				if avm_m0_read = '1' then
					state <= s1;
				elsif endfile(input_buf) then 
					state <= s3;
				end if;
			when s1 => -- Read second data word  and forward it to avm_m0_readdata and avm_m0_waitrequest
					state <= s2;
			when s2 => --  Forward previously read word to avm_m0_readdata and avm_m0_waitrequest
				if avm_m0_read = '0' and avm_m1_write = '0' then 
					state <= s0;
				else 
					state <= s2;
				end if;
			when s3 => -- Store data into output file, when avm_m1_write is logic high 
				if next_read = '1' then
					state <= s4;
				else 
					state <= s3;
				end if;
			when s4 => -- End simulation
				null;
		end case;
	end if;
	end process;

	do_states: process(clock_clk, state)
	variable read_col_from_input_buf : line;
	variable write_col_to_output_buf	: line;
	variable val_comma	: character; -- for commas 
	variable val_wr 	: bit;
	variable val_rd 	: bit_vector(31 downto 0);
	begin 
	if rising_edge(clock_clk) then
		case state is
			when s0 => 
			if avm_m0_read = '1'  and not endfile(input_buf) then
				readline(input_buf, read_col_from_input_buf);
				read(read_col_from_input_buf, val_wr);
				if val_wr = '1' then
					avm_m0_waitrequest <= '1';
				else 
					avm_m0_waitrequest <= '0';
				end if;
				read(read_col_from_input_buf, val_comma);
				read(read_col_from_input_buf, val_rd);
				avm_m0_readdata <= to_stdlogicvector(val_rd);
			end if;
			when s1 => 
			if avm_m0_read = '1'  and not endfile(input_buf) then
				readline(input_buf, read_col_from_input_buf);
				read(read_col_from_input_buf, val_wr);
				if val_wr = '1' then
					avm_m0_waitrequest <= '1';
				else 
					avm_m0_waitrequest <= '0';
				end if;
				read(read_col_from_input_buf, val_comma);
				read(read_col_from_input_buf, val_rd);
				avm_m0_readdata <= to_stdlogicvector(val_rd);
			end if;
			when s2 =>
				if val_wr = '1' then
					avm_m0_waitrequest <= '1';
				else 
					avm_m0_waitrequest <= '0';
				end if;
				avm_m0_readdata <= to_stdlogicvector(val_rd);
			when s3 =>
				if avm_m1_write = '1' then
					write(write_col_to_output_buf,to_integer(unsigned(avm_m1_writedata)));
					write(write_col_to_output_buf,string'(","));
					write(write_col_to_output_buf,to_integer(unsigned(avm_m1_address)));
				else
					writeline(output_buf, write_col_to_output_buf);
				end if;
			when s4 =>
				end_simulation <= '1';
		end case;
	end if;
	end process;
	
end architecture arch;
