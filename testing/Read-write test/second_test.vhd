-- Histogram with implemented mm masters 
-- NOTE: This version has waitrequest signals implemented, on recent tests waitrequest is not nessesery on writing at least.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity histogram is
    port (
-- Avalon memory mapped master for reading pixel value
			avm_m0_address     : out std_logic_vector(31 downto 0):= (others => '0');	-- avm_m0.address
			avm_m0_read        : out std_logic                    := '0';             --       .read
			avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m0_write       : out std_logic                    := '0';             --       .write
			avm_m0_writedata   : out std_logic_vector(31 downto 0):= (others => '0'); --       .writedata
-- Clock and reset global
			clock_clk          : in  std_logic                     := '0';             --  clock.clk
			reset_reset        : in  std_logic                     := '0';             --  reset.reset
-- Avalon mm master for writting histogram 
			avm_m1_address     : out std_logic_vector(31 downto 0):= (others => '0');  -- avm_m1.address
			avm_m1_read        : out std_logic                    := '0';              --       .read
			avm_m1_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m1_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m1_write       : out std_logic                    := '0';             --       .write
			avm_m1_writedata   : out std_logic_vector(31 downto 0):= (others => '0') --       .writedata		
);
end entity histogram;

architecture rtl of histogram is


	signal h_r_done, h_g_done, h_b_done : std_logic := '0'; -- Control signals from respective histograms
	signal h_t_done			: std_logic := '0'; -- Control signal that indicates histogram writingto on-chip memory is done
	signal pixel_ready		: std_logic := '0'; -- Control signal that indicates that pixel is ready to be send to histo_logic
	signal reset_histograms : std_logic := '0'; -- Value for reseting all 3 histograms ( could be variable)
	
	signal red_offset			: integer := 0;
	signal green_offset		: integer := 1280;
	signal blue_offset		: integer := 2560;

	signal red_address		: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal red_value			: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal green_address	  	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal green_value 		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal blue_address	  	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal blue_value 		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	constant addr_width 		: integer 	:= 32; --for generic map if it is nessesery 
	constant data_width 		: integer 	:= 8;
	
	
	type state_type is (s0, s1, s2, s3, S4, S5, S6, S7, s8, s9);
	signal state : state_type;
			
begin
	
	histo_red : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_r_done,		-- Signalises that one increment is done
			
			addr_a	=> red_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> red_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b		=> red_value
			);
	
	histo_green : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_b_done,		-- Signalises that one increment is done
			
			addr_a	=> blue_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> blue_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b		=> blue_value
			);
	
	histo_blue : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_g_done,		-- Signalises that one increment is done
			
			addr_a	=> green_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> green_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b		=> green_value
			);

	
	
process (clock_clk, reset_reset)	
	
	begin

		if reset_reset = '1' then
			state <= s0;

		elsif (rising_edge(clock_clk)) then

			case state is
				when s0=>
					if avm_m0_waitrequest = '0' then
						state <= s1;
					else
						state <= s0;
					end if;
				when s1=>
					state <= s2;
				when s2=>
					if avm_m0_waitrequest = '0' then
						state <= s3;
					else
						state <= s2;
					end if;
				when s3=>
					state <= s4;
				when s4=>
					state <= s5;
				when s5=>
					if avm_m1_waitrequest = '0' then
						state <= s6;
					else
						state <= s5;
					end if;
				when s6=>
					state <= s7;
				when s7=>
					state <= s8;
				when s8=>
					state <= s9;
				when s9=>
					state <= s0;
			end case;

		end if;
	end process;

	process (state)
	variable SOP, EOP, FIP 	:std_logic 	:= '0'; -- SOP - start of packet, EOP - end of packet, FIP - Frame in progress 
	variable first_buffer : std_logic_vector(31 downto 0);
	variable second_buffer : std_logic_vector(31 downto 0);
	
	variable packet_address	: integer 	:= 8;
	variable SOP_count : integer 			:= 0;
	variable EOP_count : integer 			:= 0;
	variable packet_counter	: integer 	:= 0;

	begin
			case state is
				when s0=>
					avm_m1_write <= '0';
					avm_m0_read <= '1';
					avm_m0_address <= (0 => '1', others => '0');
				when s1=>
					avm_m0_read <= '1';
					avm_m0_address <= (0 => '1', others => '0');
				when s2=>
					second_buffer := avm_m0_readdata;
					avm_m0_read <= '1';
					avm_m0_address <= (others => '0');
				when s3 =>
					avm_m0_read <= '1';
					avm_m0_address <= (others => '0');
				when s4 =>
					first_buffer := avm_m0_readdata;
					avm_m0_read <= '0';
					if second_buffer(0) = '1' then
						SOP_count := SOP_count + 1;
					end if;
					if second_buffer(1) = '1' then
						EOP_count := EOP_count + 1;
						packet_counter := 0;
						if packet_address < 1020 then
							packet_address := packet_address + 4;
						else 
							packet_address := 12;
						end if;
					end if;
					if  second_buffer(0) = '0' and second_buffer(1) = '0' then
						packet_counter := packet_counter + 1;
					end if;
				when s5=>
					avm_m1_write 		<= '1';
					avm_m1_address 	<= (others => '0'); 
					avm_m1_writedata 	<= first_buffer;
				when s6=>
					avm_m1_write 		<= '1';
					avm_m1_address 	<= (2 => '1', others => '0'); -- Adresa 4
					avm_m1_writedata 	<= std_logic_vector(to_unsigned(SOP_count, avm_m1_writedata'LENGTH));
				when s7=>
					avm_m1_write 		<= '1';
					avm_m1_address 	<= (3 => '1', others => '0'); -- Adresa 8
					avm_m1_writedata 	<= std_logic_vector(to_unsigned(EOP_count, avm_m1_writedata'LENGTH));
				when s8=>
					avm_m1_write 		<= '1';
					avm_m1_address 	<= std_logic_vector(to_unsigned(packet_address,avm_m1_address'LENGTH));
					avm_m1_writedata 	<= std_logic_vector(to_unsigned(packet_counter, avm_m1_writedata'LENGTH));
				when s9=>
					avm_m1_write 		<= '1';
					avm_m1_address 	<= std_logic_vector(to_unsigned(green_offset,avm_m1_address'LENGTH));
					avm_m1_writedata 	<= (0|1 => '1', others => '0');
			end case;
	end process;

end architecture rtl; 