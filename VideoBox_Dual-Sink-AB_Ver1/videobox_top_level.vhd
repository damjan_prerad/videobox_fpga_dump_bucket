library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity videobox_top_level is
	port(
		CLK_50: in std_logic;
		CLK_sdram: out std_logic;
		hps_0_ddr_mem_a       : out   std_logic_vector(14 downto 0);                    -- mem_a
		hps_0_ddr_mem_ba      : out   std_logic_vector(2 downto 0);                     -- mem_ba
		hps_0_ddr_mem_ck      : out   std_logic;                                        -- mem_ck
		hps_0_ddr_mem_ck_n    : out   std_logic;                                        -- mem_ck_n
		hps_0_ddr_mem_cke     : out   std_logic;                                        -- mem_cke
		hps_0_ddr_mem_cs_n    : out   std_logic;                                        -- mem_cs_n
		hps_0_ddr_mem_ras_n   : out   std_logic;                                        -- mem_ras_n
		hps_0_ddr_mem_cas_n   : out   std_logic;                                        -- mem_cas_n
		hps_0_ddr_mem_we_n    : out   std_logic;                                        -- mem_we_n
		hps_0_ddr_mem_reset_n : out   std_logic;                                        -- mem_reset_n
		hps_0_ddr_mem_dq      : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
		hps_0_ddr_mem_dqs     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
		hps_0_ddr_mem_dqs_n   : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
		hps_0_ddr_mem_odt     : out   std_logic;                                        -- mem_odt
		hps_0_ddr_mem_dm      : out   std_logic_vector(3 downto 0);                     -- mem_dm
		hps_0_ddr_oct_rzqin   : in    std_logic                     := 'X';             -- oct_rzqin
		LEDh2f					 : out 	std_logic_vector(9 downto 0);
		LEDlwh2f					 : out 	std_logic_vector(4 downto 0);
		
		xsdram_addr                          : out   std_logic_vector(12 downto 0);                    -- addr
		xsdram_ba                            : out   std_logic_vector(1 downto 0);                     -- ba
		xsdram_cas_n                         : out   std_logic;                                        -- cas_n
		xsdram_cke                           : out   std_logic;                                        -- cke
		xsdram_cs_n                          : out   std_logic;                                        -- cs_n
		xsdram_dq                            : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
		xsdram_dqm                           : out   std_logic_vector(1 downto 0);                     -- dqm
		xsdram_ras_n                         : out   std_logic;                                        -- ras_n
		xsdram_we_n                          : out   std_logic;                                        -- we_n
		xvga_interface_CLK                   : out   std_logic;                                        -- CLK
		xvga_interface_HS                    : out   std_logic;                                        -- HS
		xvga_interface_VS                    : out   std_logic;                                        -- VS
		xvga_interface_BLANK                 : out   std_logic;                                        -- BLANK
		xvga_interface_SYNC                  : out   std_logic;                                        -- SYNC
		xvga_interface_R                     : out   std_logic_vector(7 downto 0);                     -- R
		xvga_interface_G                     : out   std_logic_vector(7 downto 0);                     -- G
		xvga_interface_B                     : out   std_logic_vector(7 downto 0)                      -- B
);
end videobox_top_level;

architecture main of videobox_top_level is
	  component videobox_components is
        port (
            clk_clk                             : in    std_logic                     := 'X';             -- clk
            clk_sdram_clk       						: out   std_logic;                                        -- sdram_clk
            memory_mem_a                        : out   std_logic_vector(14 downto 0);                    -- mem_a
            memory_mem_ba                       : out   std_logic_vector(2 downto 0);                     -- mem_ba
            memory_mem_ck                       : out   std_logic;                                        -- mem_ck
            memory_mem_ck_n                     : out   std_logic;                                        -- mem_ck_n
            memory_mem_cke                      : out   std_logic;                                        -- mem_cke
            memory_mem_cs_n                     : out   std_logic;                                        -- mem_cs_n
            memory_mem_ras_n                    : out   std_logic;                                        -- mem_ras_n
            memory_mem_cas_n                    : out   std_logic;                                        -- mem_cas_n
            memory_mem_we_n                     : out   std_logic;                                        -- mem_we_n
            memory_mem_reset_n                  : out   std_logic;                                        -- mem_reset_n
            memory_mem_dq                       : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            memory_mem_dqs                      : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            memory_mem_dqs_n                    : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            memory_mem_odt                      : out   std_logic;                                        -- mem_odt
            memory_mem_dm                       : out   std_logic_vector(3 downto 0);                     -- mem_dm
            memory_oct_rzqin                    : in    std_logic                     := 'X';             -- oct_rzqin
            sdram_addr                          : out   std_logic_vector(12 downto 0);                    -- addr
            sdram_ba                            : out   std_logic_vector(1 downto 0);                     -- ba
            sdram_cas_n                         : out   std_logic;                                        -- cas_n
            sdram_cke                           : out   std_logic;                                        -- cke
            sdram_cs_n                          : out   std_logic;                                        -- cs_n
            sdram_dq                            : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
            sdram_dqm                           : out   std_logic_vector(1 downto 0);                     -- dqm
            sdram_ras_n                         : out   std_logic;                                        -- ras_n
            sdram_we_n                          : out   std_logic;                                        -- we_n
            vga_interface_CLK                   : out   std_logic;                                        -- CLK
            vga_interface_HS                    : out   std_logic;                                        -- HS
            vga_interface_VS                    : out   std_logic;                                        -- VS
            vga_interface_BLANK                 : out   std_logic;                                        -- BLANK
            vga_interface_SYNC                  : out   std_logic;                                        -- SYNC
            vga_interface_R                     : out   std_logic_vector(7 downto 0);                     -- R
            vga_interface_G                     : out   std_logic_vector(7 downto 0);                     -- G
            vga_interface_B                     : out   std_logic_vector(7 downto 0)                      -- B
				);
    end component videobox_components;

begin
	
	u0 : component videobox_components
        port map (
            clk_clk                             => CLK_50,                            		 --                       clk.clk
            clk_sdram_clk       						=> CLK_sdram,       		 						 --     						  clk_sdram.clk
            memory_mem_a                        => hps_0_ddr_mem_a,                        --                       memory.mem_a
            memory_mem_ba                       => hps_0_ddr_mem_ba,                       --                             .mem_ba
            memory_mem_ck                       => hps_0_ddr_mem_ck,                       --                             .mem_ck
            memory_mem_ck_n                     => hps_0_ddr_mem_ck_n,                     --                             .mem_ck_n
            memory_mem_cke                      => hps_0_ddr_mem_cke,                      --                             .mem_cke
            memory_mem_cs_n                     => hps_0_ddr_mem_cs_n,                     --                             .mem_cs_n
            memory_mem_ras_n                    => hps_0_ddr_mem_ras_n,                    --                             .mem_ras_n
            memory_mem_cas_n                    => hps_0_ddr_mem_cas_n,                    --                             .mem_cas_n
            memory_mem_we_n                     => hps_0_ddr_mem_we_n,                     --                             .mem_we_n
            memory_mem_reset_n                  => hps_0_ddr_mem_reset_n,                  --                             .mem_reset_n
            memory_mem_dq                       => hps_0_ddr_mem_dq,                       --                             .mem_dq
            memory_mem_dqs                      => hps_0_ddr_mem_dqs,                      --                             .mem_dqs
            memory_mem_dqs_n                    => hps_0_ddr_mem_dqs_n,                    --                             .mem_dqs_n
            memory_mem_odt                      => hps_0_ddr_mem_odt,                      --                             .mem_odt
            memory_mem_dm                       => hps_0_ddr_mem_dm,                       --                             .mem_dm
            memory_oct_rzqin                    => hps_0_ddr_oct_rzqin,                    --                             .oct_rzqin

				sdram_addr                          => xsdram_addr,                          --                        sdram.addr
            sdram_ba                            => xsdram_ba,                            --                             .ba
            sdram_cas_n                         => xsdram_cas_n,                         --                             .cas_n
            sdram_cke                           => xsdram_cke,                           --                             .cke
            sdram_cs_n                          => xsdram_cs_n,                          --                             .cs_n
            sdram_dq                            => xsdram_dq,                            --                             .dq
            sdram_dqm                           => xsdram_dqm,                           --                             .dqm
            sdram_ras_n                         => xsdram_ras_n,                         --                             .ras_n
            sdram_we_n                          => xsdram_we_n,                          --                             .we_n
            vga_interface_CLK                   => xvga_interface_CLK,                   --                vga_interface.CLK
            vga_interface_HS                    => xvga_interface_HS,                    --                             .HS
            vga_interface_VS                    => xvga_interface_VS,                    --                             .VS
            vga_interface_BLANK                 => xvga_interface_BLANK,                 --                             .BLANK
            vga_interface_SYNC                  => xvga_interface_SYNC,                  --                             .SYNC
            vga_interface_R                     => xvga_interface_R,                     --                             .R
            vga_interface_G                     => xvga_interface_G,                     --                             .G
            vga_interface_B                     => xvga_interface_B                      --                             .B
		  );

		  
end architecture;