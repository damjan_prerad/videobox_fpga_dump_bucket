	videobox_components u0 (
		.clk_clk             (<connected-to-clk_clk>),             //           clk.clk
		.clk_sdram_clk       (<connected-to-clk_sdram_clk>),       //     clk_sdram.clk
		.memory_mem_a        (<connected-to-memory_mem_a>),        //        memory.mem_a
		.memory_mem_ba       (<connected-to-memory_mem_ba>),       //              .mem_ba
		.memory_mem_ck       (<connected-to-memory_mem_ck>),       //              .mem_ck
		.memory_mem_ck_n     (<connected-to-memory_mem_ck_n>),     //              .mem_ck_n
		.memory_mem_cke      (<connected-to-memory_mem_cke>),      //              .mem_cke
		.memory_mem_cs_n     (<connected-to-memory_mem_cs_n>),     //              .mem_cs_n
		.memory_mem_ras_n    (<connected-to-memory_mem_ras_n>),    //              .mem_ras_n
		.memory_mem_cas_n    (<connected-to-memory_mem_cas_n>),    //              .mem_cas_n
		.memory_mem_we_n     (<connected-to-memory_mem_we_n>),     //              .mem_we_n
		.memory_mem_reset_n  (<connected-to-memory_mem_reset_n>),  //              .mem_reset_n
		.memory_mem_dq       (<connected-to-memory_mem_dq>),       //              .mem_dq
		.memory_mem_dqs      (<connected-to-memory_mem_dqs>),      //              .mem_dqs
		.memory_mem_dqs_n    (<connected-to-memory_mem_dqs_n>),    //              .mem_dqs_n
		.memory_mem_odt      (<connected-to-memory_mem_odt>),      //              .mem_odt
		.memory_mem_dm       (<connected-to-memory_mem_dm>),       //              .mem_dm
		.memory_oct_rzqin    (<connected-to-memory_oct_rzqin>),    //              .oct_rzqin
		.sdram_addr          (<connected-to-sdram_addr>),          //         sdram.addr
		.sdram_ba            (<connected-to-sdram_ba>),            //              .ba
		.sdram_cas_n         (<connected-to-sdram_cas_n>),         //              .cas_n
		.sdram_cke           (<connected-to-sdram_cke>),           //              .cke
		.sdram_cs_n          (<connected-to-sdram_cs_n>),          //              .cs_n
		.sdram_dq            (<connected-to-sdram_dq>),            //              .dq
		.sdram_dqm           (<connected-to-sdram_dqm>),           //              .dqm
		.sdram_ras_n         (<connected-to-sdram_ras_n>),         //              .ras_n
		.sdram_we_n          (<connected-to-sdram_we_n>),          //              .we_n
		.vga_interface_CLK   (<connected-to-vga_interface_CLK>),   // vga_interface.CLK
		.vga_interface_HS    (<connected-to-vga_interface_HS>),    //              .HS
		.vga_interface_VS    (<connected-to-vga_interface_VS>),    //              .VS
		.vga_interface_BLANK (<connected-to-vga_interface_BLANK>), //              .BLANK
		.vga_interface_SYNC  (<connected-to-vga_interface_SYNC>),  //              .SYNC
		.vga_interface_R     (<connected-to-vga_interface_R>),     //              .R
		.vga_interface_G     (<connected-to-vga_interface_G>),     //              .G
		.vga_interface_B     (<connected-to-vga_interface_B>)      //              .B
	);

