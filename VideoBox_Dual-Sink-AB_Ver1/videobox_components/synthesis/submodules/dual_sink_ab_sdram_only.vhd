library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dual_sink_ab_sdram_only is
	port (
		clock_clk					: in  std_logic;								--  clock.clk
		reset_reset					: in  std_logic;								--  reset.reset

		avs_s0_chipselect			: in std_logic;
		avs_s0_read					: in  std_logic;								--       .read
		avs_s0_readdata			: out std_logic_vector(15 downto 0);	--       .readdata
		avs_s0_write				: in  std_logic;								--       .write
		avs_s0_writedata			: in  std_logic_vector(15 downto 0);	--       .writedata

		asi_in0_data				: in  std_logic_vector(15 downto 0);	--  asi_in0.data
		asi_in0_ready				: out std_logic := '1';						--         .ready
		asi_in0_valid				: in  std_logic;								--         .valid
		asi_in0_startofpacket	: in std_logic;
		asi_in0_endofpacket		: in std_logic;

		asi_in1_data   			: in  std_logic_vector(15 downto 0);	--  asi_in0.data
		asi_in1_ready				: out std_logic := '0';						--         .ready
		asi_in1_valid				: in  std_logic;								--         .valid
		asi_in1_startofpacket	: in std_logic;
		asi_in1_endofpacket		: in std_logic;

		aso_out0_data				: out std_logic_vector(15 downto 0);	-- aso_out0.data
		aso_out0_ready				: in  std_logic;								--         .ready
		aso_out0_valid				: out std_logic := '0';
		aso_out0_startofpacket	: out std_logic := '0';
		aso_out0_endofpacket		: out std_logic := '0'
	);
end entity dual_sink_ab_sdram_only;

architecture rtl of dual_sink_ab_sdram_only is
	signal alpha : std_logic_vector(15 downto 0) := (others => '0');
	signal r1, r2 : std_logic_vector(4 downto 0);
	signal g1, g2 : std_logic_vector(5 downto 0);
	signal b1, b2 : std_logic_vector(4 downto 0);
	signal r_out : std_logic_vector(4 downto 0);
	signal g_out : std_logic_vector(5 downto 0);
	signal b_out : std_logic_vector(4 downto 0);
	
	signal sync_background : std_logic;
	signal sync_foreground : std_logic;
	
	signal valid : std_logic;
	
begin

	alpha_in: process(clock_clk)
	begin
		if clock_clk'event and clock_clk = '1' then
		  if avs_s0_write = '1' then
				alpha <= avs_s0_writedata;
		  end if;
		end if;
	end process;
	
	r1 <= asi_in0_data(15 downto 11);
	g1 <= asi_in0_data(10 downto 5);
	b1 <= asi_in0_data(4 downto 0);
	r2 <= asi_in1_data(15 downto 11);
	g2 <= asi_in1_data(10 downto 5);
	b2 <= asi_in1_data(4 downto 0);
	
	r_out <= std_logic_vector(to_unsigned(((to_integer(unsigned(alpha))*to_integer(unsigned(r1)) + (1000-to_integer(unsigned(alpha)))*to_integer(unsigned(r2)))/1000), r_out'length));
	g_out <= std_logic_vector(to_unsigned(((to_integer(unsigned(alpha))*to_integer(unsigned(g1)) + (1000-to_integer(unsigned(alpha)))*to_integer(unsigned(g2)))/1000), g_out'length));
	b_out <= std_logic_vector(to_unsigned(((to_integer(unsigned(alpha))*to_integer(unsigned(b1)) + (1000-to_integer(unsigned(alpha)))*to_integer(unsigned(b2)))/1000), b_out'length));
	
	asi_in0_ready <= (aso_out0_ready and valid) or sync_background;
	asi_in1_ready <= (aso_out0_ready and valid) or sync_foreground;
	
	sync_background <= (asi_in0_valid and asi_in1_valid and ((asi_in1_startofpacket and (not asi_in0_startofpacket)) or (asi_in1_endofpacket and (not asi_in0_endofpacket))));
	sync_foreground <= (asi_in0_valid and asi_in1_valid and ((asi_in0_startofpacket and (not asi_in1_startofpacket)) or (asi_in0_endofpacket and (not asi_in1_endofpacket))));

	valid <=	asi_in0_valid and asi_in1_valid and (not sync_background) and (not sync_foreground);
	
	aso_out0_startofpacket <= asi_in0_startofpacket;
	aso_out0_endofpacket <= asi_in0_endofpacket;
	aso_out0_valid <= valid;
	aso_out0_data <= r_out & g_out & b_out;
	
end architecture rtl;
