#!/bin/bash 
 
# make sure script is run from its current directory DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) cd "$DIR" 
 
sopc-create-header-files \ "hps_pio.sopcinfo" \ --single "hps_pio.h" \ --module "hps_pio" 
 
if [ ! -d "sw/hps/application/" ]; then     mkdir "sw/hps/application/" fi 
 
mv "hps_soc_system.h" "sw/hps/application/"