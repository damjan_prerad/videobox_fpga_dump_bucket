#ifndef _ALTERA_HPS_PIO_H_
#define _ALTERA_HPS_PIO_H_

/*
 * This file was automatically generated by the swinfo2header utility.
 * 
 * Created from SOPC Builder system 'hps_pio' in
 * file 'hps_pio.sopcinfo'.
 */

/*
 * This file contains macros for module 'hps_0' and devices
 * connected to the following master:
 *   h2f_lw_axi_master
 * 
 * Do not include this header file and another header file created for a
 * different module or master group at the same time.
 * Doing so may result in duplicate macro names.
 * Instead, use the system header file which has macros with unique names.
 */

/*
 * Macros for device 'switches', class 'altera_avalon_pio'
 * The macros are prefixed with 'SWITCHES_'.
 * The prefix is the slave descriptor.
 */
#define SWITCHES_COMPONENT_TYPE altera_avalon_pio
#define SWITCHES_COMPONENT_NAME switches
#define SWITCHES_BASE 0x0
#define SWITCHES_SPAN 16
#define SWITCHES_END 0xf
#define SWITCHES_BIT_CLEARING_EDGE_REGISTER 0
#define SWITCHES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define SWITCHES_CAPTURE 0
#define SWITCHES_DATA_WIDTH 10
#define SWITCHES_DO_TEST_BENCH_WIRING 0
#define SWITCHES_DRIVEN_SIM_VALUE 0
#define SWITCHES_EDGE_TYPE NONE
#define SWITCHES_FREQ 50000000
#define SWITCHES_HAS_IN 1
#define SWITCHES_HAS_OUT 0
#define SWITCHES_HAS_TRI 0
#define SWITCHES_IRQ_TYPE NONE
#define SWITCHES_RESET_VALUE 0

/*
 * Macros for device 'led_control', class 'altera_avalon_pio'
 * The macros are prefixed with 'LED_CONTROL_'.
 * The prefix is the slave descriptor.
 */
#define LED_CONTROL_COMPONENT_TYPE altera_avalon_pio
#define LED_CONTROL_COMPONENT_NAME led_control
#define LED_CONTROL_BASE 0x10
#define LED_CONTROL_SPAN 16
#define LED_CONTROL_END 0x1f
#define LED_CONTROL_BIT_CLEARING_EDGE_REGISTER 0
#define LED_CONTROL_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LED_CONTROL_CAPTURE 0
#define LED_CONTROL_DATA_WIDTH 10
#define LED_CONTROL_DO_TEST_BENCH_WIRING 0
#define LED_CONTROL_DRIVEN_SIM_VALUE 0
#define LED_CONTROL_EDGE_TYPE NONE
#define LED_CONTROL_FREQ 50000000
#define LED_CONTROL_HAS_IN 0
#define LED_CONTROL_HAS_OUT 1
#define LED_CONTROL_HAS_TRI 0
#define LED_CONTROL_IRQ_TYPE NONE
#define LED_CONTROL_RESET_VALUE 0

/*
 * Macros for device 'ab_avalon_interface_0', class 'ab_avalon_interface'
 * The macros are prefixed with 'AB_AVALON_INTERFACE_0_'.
 * The prefix is the slave descriptor.
 */
#define AB_AVALON_INTERFACE_0_COMPONENT_TYPE ab_avalon_interface
#define AB_AVALON_INTERFACE_0_COMPONENT_NAME ab_avalon_interface_0
#define AB_AVALON_INTERFACE_0_BASE 0x20
#define AB_AVALON_INTERFACE_0_SPAN 2
#define AB_AVALON_INTERFACE_0_END 0x21


#endif /* _ALTERA_HPS_PIO_H_ */
