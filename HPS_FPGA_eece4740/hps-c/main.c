#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"

//#include "soc_cv_av/socal/socal.h"
//#include "soc_cv_av/socal/hps.h"
//#include "soc_cv_av/socal/alt_gpio.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"

#include "hps_0.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

int main(int argc, char* argv[])
{
	void*	virtual_base;
	int 	fd;
	void*	addr_seven_seg;

	(void)argc;
	(void)argv;

	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span
	if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		printf("ERROR: could not open \"/dev/mem\"...\n");
		
		return 1;
	}

	virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, HW_REGS_BASE);

	if (virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}

	addr_seven_seg = virtual_base + ((unsigned long)(ALT_LWFPGASLVS_OFST + OUTPUT_SEVEN_SEGMENT_BASE) & (unsigned long)(HW_REGS_MASK));

	// Enable all six digits of the seven-segment display
	alt_write_word(addr_seven_seg+8, 0x3F);

	// Set the display brightness
	alt_write_word(addr_seven_seg+4, 0x150);

	while (1) {
		int i;

		for (i = 0; i < 0xffffff; i++) {
			alt_write_word(addr_seven_seg+0, i);
		
			usleep(10 * 1000);
		}
	}

	// Clean up our memory mapping and exit
	if (munmap(virtual_base, HW_REGS_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}

	close(fd);

	return 0;
}
