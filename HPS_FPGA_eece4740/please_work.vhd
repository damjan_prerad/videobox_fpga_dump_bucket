LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY hex7seg IS
	PORT ( 
		hex : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		display : OUT STD_LOGIC_VECTOR(0 TO 6)
	);
END hex7seg;

ARCHITECTURE Behavior OF hex7seg IS
BEGIN
-- – 0 –
-- 5 | | 1
-- – 6 –
-- 4 | | 2
-- – 3 –
	PROCESS (hex)
	BEGIN
		CASE hex IS
			WHEN "0000" => display <= "0000001";
			WHEN "0001" => display <= "1001111";
			WHEN "0010" => display <= "0010010";
			WHEN "0011" => display <= "0000110";
			WHEN "0100" => display <= "1001100";
			WHEN "0101" => display <= "0100100";
			WHEN "0110" => display <= "0100000";
			WHEN "0111" => display <= "0001111";
			WHEN "1000" => display <= "0000000";
			WHEN "1001" => display <= "0001100";
			WHEN "1010" => display <= "0001000";
			WHEN "1011" => display <= "1100000";
			WHEN "1100" => display <= "0110001";
			WHEN "1101" => display <= "1000010";
			WHEN "1110" => display <= "0110000";
			WHEN "1111" => display <= "0111000";
		END CASE;
	END PROCESS;
END Behavior;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity please_work is
	port (
		clock_50 : in std_logic;
		key : in std_logic_vector(0 downto 0);
		hex0 : out std_logic_vector(0 to 6);
		hex1 : out std_logic_vector(0 to 6);
		hex2 : out std_logic_vector(0 to 6);
		hex3 : out std_logic_vector(0 to 6);
		
      hps_0_ddr_mem_a       : out   std_logic_vector(14 downto 0);                    -- mem_a
      hps_0_ddr_mem_ba      : out   std_logic_vector(2 downto 0);                     -- mem_ba
      hps_0_ddr_mem_ck      : out   std_logic;                                        -- mem_ck
		hps_0_ddr_mem_ck_n    : out   std_logic;                                        -- mem_ck_n
		hps_0_ddr_mem_cke     : out   std_logic;                                        -- mem_cke
		hps_0_ddr_mem_cs_n    : out   std_logic;                                        -- mem_cs_n
		hps_0_ddr_mem_ras_n   : out   std_logic;                                        -- mem_ras_n
		hps_0_ddr_mem_cas_n   : out   std_logic;                                        -- mem_cas_n
		hps_0_ddr_mem_we_n    : out   std_logic;                                        -- mem_we_n
		hps_0_ddr_mem_reset_n : out   std_logic;                                        -- mem_reset_n
		hps_0_ddr_mem_dq      : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
		hps_0_ddr_mem_dqs     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
		hps_0_ddr_mem_dqs_n   : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
		hps_0_ddr_mem_odt     : out   std_logic;                                        -- mem_odt
		hps_0_ddr_mem_dm      : out   std_logic_vector(3 downto 0);                     -- mem_dm
		hps_0_ddr_oct_rzqin   : in    std_logic                     := 'X';             -- oct_rzqin
		LEDS: out STD_LOGIC_VECTOR(9 DOWNTO 0);
		SWITCHES: in STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
end please_work;

architecture rtl of please_work is
signal to_hex : std_logic_vector(15 downto 0);
component hps_pio is
        port (
            clk_clk               : in    std_logic                     := 'X';             -- clk
            hps_0_ddr_mem_a       : out   std_logic_vector(14 downto 0);                    -- mem_a
            hps_0_ddr_mem_ba      : out   std_logic_vector(2 downto 0);                     -- mem_ba
            hps_0_ddr_mem_ck      : out   std_logic;                                        -- mem_ck
            hps_0_ddr_mem_ck_n    : out   std_logic;                                        -- mem_ck_n
            hps_0_ddr_mem_cke     : out   std_logic;                                        -- mem_cke
            hps_0_ddr_mem_cs_n    : out   std_logic;                                        -- mem_cs_n
            hps_0_ddr_mem_ras_n   : out   std_logic;                                        -- mem_ras_n
            hps_0_ddr_mem_cas_n   : out   std_logic;                                        -- mem_cas_n
            hps_0_ddr_mem_we_n    : out   std_logic;                                        -- mem_we_n
            hps_0_ddr_mem_reset_n : out   std_logic;                                        -- mem_reset_n
            hps_0_ddr_mem_dq      : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            hps_0_ddr_mem_dqs     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            hps_0_ddr_mem_dqs_n   : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            hps_0_ddr_mem_odt     : out   std_logic;                                        -- mem_odt
            hps_0_ddr_mem_dm      : out   std_logic_vector(3 downto 0);                     -- mem_dm
            hps_0_ddr_oct_rzqin   : in    std_logic                     := 'X';             -- oct_rzqin
            to_hex_readdata       : out   std_logic_vector(15 downto 0);                     -- readdata
				led_control_export    : out   std_logic_vector(9 downto 0); 
				switch_state_export   : in    std_logic_vector(9 downto 0)  := (others => 'X');
				to_hex_read_signal    : in    std_logic_vector(7 downto 0)  := (others => 'X')  -- read_signal
        );
    end component hps_pio;
	 
	component hex7seg is
		port (
			hex : in std_logic_vector(3 downto 0);
			display : out std_logic_vector(0 to 6)
		);
	end component hex7seg;

begin

		u0 : component hps_pio
			port map (
				clk_clk               => clock_50,               --       clk.clk
            hps_0_ddr_mem_a       => hps_0_ddr_mem_a,       -- hps_0_ddr.mem_a
            hps_0_ddr_mem_ba      => hps_0_ddr_mem_ba,      --          .mem_ba
            hps_0_ddr_mem_ck      => hps_0_ddr_mem_ck,      --          .mem_ck
            hps_0_ddr_mem_ck_n    => hps_0_ddr_mem_ck_n,    --          .mem_ck_n
            hps_0_ddr_mem_cke     => hps_0_ddr_mem_cke,     --          .mem_cke
            hps_0_ddr_mem_cs_n    => hps_0_ddr_mem_cs_n,    --          .mem_cs_n
            hps_0_ddr_mem_ras_n   => hps_0_ddr_mem_ras_n,   --          .mem_ras_n
            hps_0_ddr_mem_cas_n   => hps_0_ddr_mem_cas_n,   --          .mem_cas_n
            hps_0_ddr_mem_we_n    => hps_0_ddr_mem_we_n,    --          .mem_we_n
            hps_0_ddr_mem_reset_n => hps_0_ddr_mem_reset_n, --          .mem_reset_n
            hps_0_ddr_mem_dq      => hps_0_ddr_mem_dq,      --          .mem_dq
            hps_0_ddr_mem_dqs     => hps_0_ddr_mem_dqs,     --          .mem_dqs
            hps_0_ddr_mem_dqs_n   => hps_0_ddr_mem_dqs_n,   --          .mem_dqs_n
            hps_0_ddr_mem_odt     => hps_0_ddr_mem_odt,     --          .mem_odt
            hps_0_ddr_mem_dm      => hps_0_ddr_mem_dm,      --          .mem_dm
            hps_0_ddr_oct_rzqin   => hps_0_ddr_oct_rzqin,   --          .oct_rzqin
            to_hex_readdata       => to_hex,        --    to_hex.readdata
				led_control_export    => LEDS,     -- led_control.export
				switch_state_export   => SWITCHES,    -- switch_state.export
				to_hex_read_signal    => SWITCHES(7 downto 0)     --             .read_signal
        );
		  
--		to_hex <= "1111000011001100";
		h0 : hex7seg port map (to_hex(3 downto 0), hex0);
		h1 : hex7seg port map (to_hex(7 downto 4), hex1);
		h2 : hex7seg port map (to_hex(11 downto 8), hex2);
		h3 : hex7seg port map (to_hex(15 downto 12), hex3);
end rtl;