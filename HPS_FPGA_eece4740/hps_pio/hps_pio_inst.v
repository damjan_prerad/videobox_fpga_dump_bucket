	hps_pio u0 (
		.clk_clk               (<connected-to-clk_clk>),               //          clk.clk
		.hps_0_ddr_mem_a       (<connected-to-hps_0_ddr_mem_a>),       //    hps_0_ddr.mem_a
		.hps_0_ddr_mem_ba      (<connected-to-hps_0_ddr_mem_ba>),      //             .mem_ba
		.hps_0_ddr_mem_ck      (<connected-to-hps_0_ddr_mem_ck>),      //             .mem_ck
		.hps_0_ddr_mem_ck_n    (<connected-to-hps_0_ddr_mem_ck_n>),    //             .mem_ck_n
		.hps_0_ddr_mem_cke     (<connected-to-hps_0_ddr_mem_cke>),     //             .mem_cke
		.hps_0_ddr_mem_cs_n    (<connected-to-hps_0_ddr_mem_cs_n>),    //             .mem_cs_n
		.hps_0_ddr_mem_ras_n   (<connected-to-hps_0_ddr_mem_ras_n>),   //             .mem_ras_n
		.hps_0_ddr_mem_cas_n   (<connected-to-hps_0_ddr_mem_cas_n>),   //             .mem_cas_n
		.hps_0_ddr_mem_we_n    (<connected-to-hps_0_ddr_mem_we_n>),    //             .mem_we_n
		.hps_0_ddr_mem_reset_n (<connected-to-hps_0_ddr_mem_reset_n>), //             .mem_reset_n
		.hps_0_ddr_mem_dq      (<connected-to-hps_0_ddr_mem_dq>),      //             .mem_dq
		.hps_0_ddr_mem_dqs     (<connected-to-hps_0_ddr_mem_dqs>),     //             .mem_dqs
		.hps_0_ddr_mem_dqs_n   (<connected-to-hps_0_ddr_mem_dqs_n>),   //             .mem_dqs_n
		.hps_0_ddr_mem_odt     (<connected-to-hps_0_ddr_mem_odt>),     //             .mem_odt
		.hps_0_ddr_mem_dm      (<connected-to-hps_0_ddr_mem_dm>),      //             .mem_dm
		.hps_0_ddr_oct_rzqin   (<connected-to-hps_0_ddr_oct_rzqin>),   //             .oct_rzqin
		.led_control_export    (<connected-to-led_control_export>),    //  led_control.export
		.switch_state_export   (<connected-to-switch_state_export>),   // switch_state.export
		.to_hex_readdata       (<connected-to-to_hex_readdata>),       //       to_hex.readdata
		.to_hex_read_signal    (<connected-to-to_hex_read_signal>)     //             .read_signal
	);

