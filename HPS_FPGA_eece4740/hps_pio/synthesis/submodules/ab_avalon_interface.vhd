library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ab_avalon_interface is
	port (
		avs_s0_chipselect  : in std_logic;
		avs_s0_read        : in  std_logic;             --       .read
		avs_s0_readdata    : out std_logic_vector(15 downto 0);                    --       .readdata
		avs_s0_write       : in  std_logic;             --       .write
		avs_s0_writedata   : in  std_logic_vector(15 downto 0); --       .writedata
		clock_clk          : in  std_logic;             --  clock.clk
		reset_reset        : in  std_logic;              --  reset.reset
		export : out std_logic_vector(15 downto 0);
		read_signal : in std_logic_vector(7 downto 0)
	);
end entity ab_avalon_interface;

architecture rtl of ab_avalon_interface is
	signal int_data : std_logic_vector(15 downto 0) := (others => '0');
	component alpha_blender
		port (
			clock, resetn : in std_logic;
			alpha : in integer
		);
	end component;
begin
	ab_instance : alpha_blender port map(clock_clk, reset_reset, to_integer(unsigned(int_data)));
	
	write_proc: process(clock_clk)
	begin
    if rising_edge(clock_clk) then
        if avs_s0_write = '1' then
            int_data <= avs_s0_writedata;
				export <= int_data;
        end if;
        -- reset statement
--        if reset_reset = '1' then
--            int_data <= (others => '0');
--        end if;
    end if;
	end process;

	read_proc: process(clock_clk)
	begin
		if rising_edge(clock_clk) then
			if avs_s0_read = '1' then
            avs_s0_readdata <= std_logic_vector(to_unsigned(to_integer(unsigned(read_signal)),avs_s0_readdata'length));
			end if;
        -- reset statement
--			if reset_reset = '1' then
--            avs_s0_readdata <= (others => '0');
--			end if;
		end if;
	end process;
	--int_data <= avs_s0_writedata;
--	export <= int_data;
end architecture rtl;