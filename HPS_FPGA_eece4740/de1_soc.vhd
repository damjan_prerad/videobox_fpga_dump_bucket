library ieee;
--library soc_system;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all; 
use ieee.numeric_std.all;
--use soc_system.all;

entity hps_fpga_hex is
    port 
    (
        CLOCK_50					 : in std_logic;
		  HPS_DDR3_ADDR          : out   std_logic_vector(14 downto 0);
        HPS_DDR3_BA            : out   std_logic_vector(2 downto 0);
        HPS_DDR3_CAS_N         : out   std_logic;
        HPS_DDR3_CK_N          : out   std_logic;
        HPS_DDR3_CK_P          : out   std_logic;
        HPS_DDR3_CKE           : out   std_logic;
        HPS_DDR3_CS_N          : out   std_logic;
        HPS_DDR3_DM            : out   std_logic_vector(3 downto 0);
        HPS_DDR3_DQ            : inout std_logic_vector(31 downto 0);
        HPS_DDR3_DQS_N         : inout std_logic_vector(3 downto 0);
        HPS_DDR3_DQS_P         : inout std_logic_vector(3 downto 0);
        HPS_DDR3_ODT           : out   std_logic;
        HPS_DDR3_RAS_N         : out   std_logic;
        HPS_DDR3_RESET_N       : out   std_logic;
        HPS_DDR3_RZQ           : in    std_logic;
        HPS_DDR3_WE_N          : out   std_logic;
        HEX0						 : out std_logic_vector(6 downto 0);

    );
end entity hps_fpga_hex;

architecture rtl of hps_fpga_hex is       
	signal prescaler: integer := 0;
	signal counter: integer := 0;

	component hps_pio is
        port (
            clk_clk                 : in    std_logic                     := 'X';             -- clk
            hex_0_export            : out   std_logic_vector(6 downto 0);                     -- export
            hps_0_ddr_mem_a         : out   std_logic_vector(14 downto 0);                    -- mem_a
            hps_0_ddr_mem_ba        : out   std_logic_vector(2 downto 0);                     -- mem_ba
            hps_0_ddr_mem_ck        : out   std_logic;                                        -- mem_ck
            hps_0_ddr_mem_ck_n      : out   std_logic;                                        -- mem_ck_n
            hps_0_ddr_mem_cke       : out   std_logic;                                        -- mem_cke
            hps_0_ddr_mem_cs_n      : out   std_logic;                                        -- mem_cs_n
            hps_0_ddr_mem_ras_n     : out   std_logic;                                        -- mem_ras_n
            hps_0_ddr_mem_cas_n     : out   std_logic;                                        -- mem_cas_n
            hps_0_ddr_mem_we_n      : out   std_logic;                                        -- mem_we_n
            hps_0_ddr_mem_reset_n   : out   std_logic;                                        -- mem_reset_n
            hps_0_ddr_mem_dq        : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            hps_0_ddr_mem_dqs       : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            hps_0_ddr_mem_dqs_n     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            hps_0_ddr_mem_odt       : out   std_logic;                                        -- mem_odt
            hps_0_ddr_mem_dm        : out   std_logic_vector(3 downto 0);                     -- mem_dm
            hps_0_ddr_oct_rzqin     : in    std_logic                     := 'X';             -- oct_rzqin
        );
	end component hps_pio;

begin
		
    u0 : component hps_pio
        port map (
            clk_clk               => CLOCK_50,               --       clk.clk
            hps_0_ddr_mem_a       => HPS_DDR3_ADDR,       -- hps_0_ddr.mem_a
            hps_0_ddr_mem_ba      => HPS_DDR3_BA,      --          .mem_ba
            hps_0_ddr_mem_ck      => HPS_DDR3_CK_P,      --          .mem_ck
            hps_0_ddr_mem_ck_n    => HPS_DDR3_CK_N,    --          .mem_ck_n
            hps_0_ddr_mem_cke     => HPS_DDR3_CKE,     --          .mem_cke
            hps_0_ddr_mem_cs_n    => HPS_DDR3_CS_N,    --          .mem_cs_n
            hps_0_ddr_mem_ras_n   => HPS_DDR3_RAS_N,   --          .mem_ras_n
            hps_0_ddr_mem_cas_n   => HPS_DDR3_CAS_N,   --          .mem_cas_n
            hps_0_ddr_mem_we_n    => HPS_DDR3_WE_N,    --          .mem_we_n
            hps_0_ddr_mem_reset_n => HPS_DDR3_RESET_N, --          .mem_reset_n
            hps_0_ddr_mem_dq      => HPS_DDR3_DQ,      --          .mem_dq
            hps_0_ddr_mem_dqs     => HPS_DDR3_DQS_P,     --          .mem_dqs
            hps_0_ddr_mem_dqs_n   => HPS_DDR3_DQS_N,   --          .mem_dqs_n
            hps_0_ddr_mem_odt     => HPS_DDR3_ODT,     --          .mem_odt
            hps_0_ddr_mem_dm      => HPS_DDR3_DM,      --          .mem_dm
            hps_0_ddr_oct_rzqin   => HPS_DDR3_RZQ,   --          .oct_rzqin
            hex_0_export          => HEX0,           --     hex_0.export
        );
end architecture rtl;
