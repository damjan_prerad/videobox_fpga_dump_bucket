-- new_component.vhd

-- This file was auto-generated as a prototype implementation of a module
-- created in component editor.  It ties off all outputs to ground and
-- ignores all inputs.  It needs to be edited to make it do something
-- useful.
-- 
-- This file will not be automatically regenerated.  You should check it in
-- to your version control system if you want to keep it.

-- TODO: Dopisati logiku za MM_m0, odnosno upisivanje u onchip memoriju

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity read_test is
    port (
-- Avalon memory mapped master for reading pixel value
			avm_m0_address     : out std_logic_vector(31 downto 0);                     -- avm_m0.address
			avm_m0_read        : out std_logic;                                        --       .read
			avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m0_write       : out std_logic;                                        --       .write
			avm_m0_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
-- Clock and reset global
			clock_clk          : in  std_logic                     := '0';             --  clock.clk
			reset_reset        : in  std_logic                     := '0';             --  reset.reset
-- Avalon mm master for writting histogram 
			avm_m1_address     : out std_logic_vector(31 downto 0);                     -- avm_m1.address
			avm_m1_read        : out std_logic;                                        --       .read
			avm_m1_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m1_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m1_write       : out std_logic;                                        --       .write
			avm_m1_writedata   : out std_logic_vector(31 downto 0)                    --       .writedata		
);
end entity read_test;

architecture rtl of read_test is
	
	-- Build an enumerated type for the state machine
	type read_states is (w0, w1, w2, s0, s1, s2, s3, s4, s5, s6, s7, s8); -- ps - previous state 

	-- Register to hold the current state
	signal state : read_states := w0;  -- states from memory mapped master 1 for reading 

	signal SOP, EOP, FIP 	:std_logic := '0'; -- SOP - start of packet, EOP - end of packet, FIP - Frame in progress 
	signal data1, data2 	:std_logic_vector(31 downto 0) := (others => '0'); -- for reading data values/writing

	signal h_r_done, h_g_done, h_b_done : std_logic := '0'; -- Control signals from respective histograms
	signal h_t_done			: std_logic := '0'; -- Control signal that indicates histogram writingto on-chip memory is done
	
	signal pixel_ready		: std_logic := '0'; -- Control signal that indicates that pixel is ready to be send to histo_logic
	signal reset_histograms : std_logic := '0'; -- Value for reseting all 3 histograms ( could be variable)
		
	signal red_offset		: integer := 0;
	signal green_offset		: integer := 1280;
	signal blue_offset		: integer := 2560;

	signal red_address		: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal red_value		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal green_address	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal green_value 		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal blue_address	  	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal blue_value 		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal addr_width 		: integer := 32; --for generic map if it is nessesery 
	signal data_width 		: integer := 8;
	
	signal write_address 	: std_logic_vector(7 downto 0) 	:= (others => '0');
	signal address 			: integer	:= 1;

begin
	
	histo_red : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_r_done,		-- Signalises that one increment is done
			
			addr_a	=> red_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> red_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b	=> red_value
			);
	
	histo_green : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_b_done,		-- Signalises that one increment is done
			
			addr_a	=> blue_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> blue_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b	=> blue_value
			);
	
	histo_blue : entity work.histo_logic
	port map(
			clk		=> clock_clk,
			rst		=> reset_histograms,
			
			h_ready	=>	pixel_ready, 	-- Signal that starts increment, it is 1 when address (pixel value) is stable and sent to the addr_a 
			h_done	=> h_g_done,		-- Signalises that one increment is done
			
			addr_a	=> green_address, 	-- Actual pixel component value that we will parse (if it is 5-6-5 first 3 (2) bits are gonna be 0)
			addr_b 	=> green_address, 	-- Address used for histogram reading from port b, no need for two  
			out_b	=> green_value
			);
	
	process (clock_clk)
	begin
		if(rising_edge(clock_clk)) then 
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when w0 =>
					if avm_m1_waitrequest = '0' then
						state <= w1;
					else 
						state <= w0;
					end if;
				when w1 =>
					if avm_m1_waitrequest = '0' then
						state <= w2;
					else 
						state <= w1;
					end if;
				when w2	=>
					if avm_m1_waitrequest = '0' then
						state <= s0;
					else 
						state <= w2;
					end if;
				when s0 =>		-- State s0 is used for reseting memory and reseting h_t_done signal
					state <= s1; 
				when s1 =>		-- State s1 is used for lowering reset signal and reseting other signals like, read and write signals
					if avm_m0_waitrequest = '0' then
						state <= s2;
					else
						state <= s1;
					end if;
				when s2 =>		-- State s2 is used for setting read signal to the high value, transition on next rising edge 
					if avm_m0_waitrequest = '0' then 
						state <= s3;
					else
						state <= s2;
					end if;
				when s3 =>		-- State s3 is used for reading first 32 bits of data
					if avm_m0_waitrequest = '0' then 
						if SOP = '0' and FIP = '0'  then -- Because it shouldn't be paralel
							state <= s1;
						elsif SOP = '1' or FIP = '1' then 
							state <= s4;
						end if;
					else 
						state <= s3;
					end if;
				when s4 =>		-- State s4 is used for setting up new address
					if avm_m1_waitrequest = '0' and EOP = '1' then 
						state <= s5;
					elsif avm_m0_waitrequest = '0' and EOP = '0' then 
						state <= s1;
					else 
						state <= s4;
					end if;
				when s5 =>		-- State s5 is used for reading next 32 bits of data
					if avm_m1_waitrequest = '0' then 
						state <= s6;
					else
						state <= s5;
					end if;
				when s6 =>		-- State s6 is used for setting up SOP and EOP
					if avm_m1_waitrequest = '0' then 
						state <= s7;
					else
						state <= s6;
					end if;
				when s7 =>		-- State s7 is used for setting up FIP and data flow/ this could maybe be together with s6
					if avm_m1_waitrequest = '0' then 
						state <= s8;
					else
						state <= s7;
					end if;
				when s8 =>		-- State s8 is used for incrementing histogram values 
					if avm_m1_waitrequest = '0' then 
						state <= s5;
					else
						state <= s8;
					end if;

			end case;
		end if;	
	end process;

	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state)
-- variable data1, data2 : std_logic_vector (31 downto 0) := (others => '0');	
	
	begin
		case state is
			when w0 => 
				avm_m1_read			<= '0';
				avm_m1_write		<= '0';
				avm_m1_address 		<= ( 0 => '1', others => '0');	-- set address to 1 
				avm_m1_writedata	<= "00000000000000000000000000000011"; 
			when w1 => 	-- duplicate just
				avm_m1_read			<= '0';
				avm_m1_write		<= '0';
				avm_m1_address 		<= ( 0 => '1', others => '0');	-- set address to 1 
				avm_m1_writedata	<= "00000000000000000000000000000011"; 
			when w2 =>
				avm_m1_write 		<= '1'; 	-- Write 3 to the memory address 1 and then go to state s0
			when s0 =>
				avm_m0_read			<= '0';
				avm_m0_write		<= '0';
				avm_m0_address 	<= ( 0 => '1', others => '0');	-- Or base value 
				avm_m1_read			<= '0';
				avm_m1_write		<= '0';
				avm_m1_address 	<= (others => '0');	-- Or base value
				FIP 					<= '0'; -- Reset file in progress flag so we are sure it has value at the begining
				reset_histograms 	<= '1';
				address 				<= 0; 	-- Reset address for on-chip memory 
			when s1 => -- First step in cycle, set read to 1, and read_address to base + offset 1 
				reset_histograms 	<= '0';
				avm_m0_read			<= '1';
				avm_m0_write		<= '0';
				avm_m0_address 	<= ( 0 => '1', others => '0');	-- base value + offset  
				avm_m1_read			<= '0';
				avm_m1_write		<= '0';
				avm_m1_address 	<= (others => '0');	-- Or base value
			when s2 => -- Read data and change address 
				SOP <= avm_m0_readdata(0);
				EOP <= avm_m0_readdata(1);
				avm_m0_address <= (others => '0');
			when s3 =>  -- Read pixel values and cancel read flag
				data1 <= avm_m0_readdata;
				avm_m0_read <= '0';
			when s4 =>	-- Set FIP
				if(EOP = '1') then
					FIP <= '0';
				elsif(SOP = '1') then
					FIP <= '1';
				end if;
				address <= address + 1;
			when s5 =>
				avm_m1_address		<= "00000000000000000000000000000000";
				avm_m1_writedata	<= std_logic_vector(to_unsigned(address,addr_width)); 
			when s6 =>
				avm_m1_write 	<= '1';
			when s7 =>
				h_r_done <= '1';
			when s8 => 
				avm_m1_write	<= '0';
			end case;
	end process;

	 

end architecture rtl; -- of new_component