#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdint.h>
#include "HPS_VGA.h"
// VGA, keys, and LEDs

#define SWAP(X,Y) do{int temp=X; X=Y; Y=temp;}while(0) 

/* function prototypes */
void VGA_text (int, int, char *);
void VGA_box (int, int, int, int, short);
void VGA_line(int, int, int, int, short) ;

// virtual to real address pointers

volatile unsigned int * vga_pixel_ptr = NULL ;
void *vga_pixel_virtual_base;


int fd;

int main(void)
{
	// Declare volatile pointers to I/O registers (volatile 	// means that IO load and store instructions will be used 	// to access these pointer locations, 
	// instead of regular memory loads and stores) 

  	
	// === need to mmap: =======================
	// FPGA_CHAR_BASE
	// FPGA_ONCHIP_BASE      
	// HW_REGS_BASE        
  
	// === get FPGA addresses ==================
    // Open /dev/mem
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) 	{
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}

	// === get VGA pixel addr ====================
	// get virtual addr that maps to physical
	vga_pixel_virtual_base = mmap( NULL, SDRAM_SPAN, ( 	PROT_READ 	| PROT_WRITE ), MAP_SHARED, fd, 			SDRAM_BASE);	// Umjesto MILANOV_POKUSAJ staviti SDRAM_BASE, eventualno pokusati staviti to umjesto null
	if( vga_pixel_virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap3() failed...\n" );
		close( fd );
		return(1);
	}
    
 
	short color ;
	int i;
	/*
	for(i = 0; i < 160 * 640; i++)
	{
		*((uint16_t *)(vga_pixel_virtual_base) + i) = 0xf800;
	}
	for(i = 0; i < 160 * 640; i++)
	{
		*((uint16_t *)(vga_pixel_virtual_base) + i + 160 * 640) = 0x07e0;
	}
	for(i = 0; i < 160 * 640; i++)
	{
		*((uint16_t *)(vga_pixel_virtual_base) + i + 2 * 160 * 640) = 0x001f;
	}*/
	int a;
	printf("Unesi a:\n");
	scanf("%d", &a);
	for(i = 0; i < a; i++)
	{
		printf("%x ", *((uint16_t *)(vga_pixel_virtual_base) + i + BUFFER_START));
		if(i % 640 == 0) printf("\n");
	}
}
