#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdint.h>
#include "HPS_VGA.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

#define ALT_AXI_FPGASLVS_OFST (0xC0000000) // axi_master
#define HW_FPGA_AXI_SPAN (0x40000000) // Bridge span 1GB
#define HW_FPGA_AXI_MASK ( HW_FPGA_AXI_SPAN - 1 )


volatile unsigned int * vga_pixel_ptr = NULL ;
void *vga_pixel_virtual_base;

void *h2p_lw_virtual_base;

int fd;

int main(void)
{
    // Open /dev/mem
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) 	{
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}

	vga_pixel_virtual_base = mmap( NULL, HW_FPGA_AXI_SPAN, ( 	PROT_READ 	| PROT_WRITE ), MAP_SHARED, fd, 			ALT_AXI_FPGASLVS_OFST);	// Umjesto MILANOV_POKUSAJ staviti SDRAM_BASE, eventualno pokusati staviti to umjesto null
	if( vga_pixel_virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap3() failed...\n" );
		close( fd );
		return(1);
	}
    
	h2p_lw_virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );
	if( h2p_lw_virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap1() failed...\n" );
		close( fd );
		return(1);
	}
 
	short color ;
	int i;
	int a;
	printf("Unesi koji word da cita:\n");
	scanf("%d", &a);
	printf("%d\n", *((uint16_t *)(h2p_lw_virtual_base) + a));
	
	
	printf("Unesi koliko da pise:\n");
	scanf("%d", &a);
	
	for(i = 0; i < a; i++)
	{
		*((uint16_t *)(vga_pixel_virtual_base)) = 0x07e0;
	}
	/*
	for(i = 0; i < 160 * 640; i++)
	{
		*((uint16_t *)(vga_pixel_virtual_base) + i + 2 * 160 * 640) = 0x001f;
	}*/

}
