/* Hex of RGB565 to RGB888 */

#include <stdio.h>
#include <stdint.h>

void main() {
	
	uint16_t hex;
	
	printf("Unesi RGB565 u hex obliku\n");
	scanf("%x",&hex);
	uint32_t color = 0;
	uint8_t r = ((((hex >> 11) & 0x1F) * 527) + 23) >> 6;
	uint8_t g = ((((hex >> 5) & 0x3F) * 259) + 33) >> 6;
	uint8_t b = (((hex & 0x1F) * 527) + 23) >> 6;
	color = r << 16 | g << 8 | b;
	printf("Boja je: %x",color);
	
}