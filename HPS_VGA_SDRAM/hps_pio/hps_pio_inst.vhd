	component hps_pio is
		port (
			clk_clk                             : in    std_logic                     := 'X';             -- clk
			ledh2f_external_connection_export   : out   std_logic_vector(4 downto 0);                     -- export
			ledlwh2f_external_connection_export : out   std_logic_vector(4 downto 0);                     -- export
			memory_mem_a                        : out   std_logic_vector(14 downto 0);                    -- mem_a
			memory_mem_ba                       : out   std_logic_vector(2 downto 0);                     -- mem_ba
			memory_mem_ck                       : out   std_logic;                                        -- mem_ck
			memory_mem_ck_n                     : out   std_logic;                                        -- mem_ck_n
			memory_mem_cke                      : out   std_logic;                                        -- mem_cke
			memory_mem_cs_n                     : out   std_logic;                                        -- mem_cs_n
			memory_mem_ras_n                    : out   std_logic;                                        -- mem_ras_n
			memory_mem_cas_n                    : out   std_logic;                                        -- mem_cas_n
			memory_mem_we_n                     : out   std_logic;                                        -- mem_we_n
			memory_mem_reset_n                  : out   std_logic;                                        -- mem_reset_n
			memory_mem_dq                       : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
			memory_mem_dqs                      : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
			memory_mem_dqs_n                    : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
			memory_mem_odt                      : out   std_logic;                                        -- mem_odt
			memory_mem_dm                       : out   std_logic_vector(3 downto 0);                     -- mem_dm
			memory_oct_rzqin                    : in    std_logic                     := 'X';             -- oct_rzqin
			sdram_addr                          : out   std_logic_vector(12 downto 0);                    -- addr
			sdram_ba                            : out   std_logic_vector(1 downto 0);                     -- ba
			sdram_cas_n                         : out   std_logic;                                        -- cas_n
			sdram_cke                           : out   std_logic;                                        -- cke
			sdram_cs_n                          : out   std_logic;                                        -- cs_n
			sdram_dq                            : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
			sdram_dqm                           : out   std_logic_vector(1 downto 0);                     -- dqm
			sdram_ras_n                         : out   std_logic;                                        -- ras_n
			sdram_we_n                          : out   std_logic;                                        -- we_n
			sdram_clk_clk                       : out   std_logic;                                        -- clk
			vga_interface_CLK                   : out   std_logic;                                        -- CLK
			vga_interface_HS                    : out   std_logic;                                        -- HS
			vga_interface_VS                    : out   std_logic;                                        -- VS
			vga_interface_BLANK                 : out   std_logic;                                        -- BLANK
			vga_interface_SYNC                  : out   std_logic;                                        -- SYNC
			vga_interface_R                     : out   std_logic_vector(7 downto 0);                     -- R
			vga_interface_G                     : out   std_logic_vector(7 downto 0);                     -- G
			vga_interface_B                     : out   std_logic_vector(7 downto 0)                      -- B
		);
	end component hps_pio;

	u0 : component hps_pio
		port map (
			clk_clk                             => CONNECTED_TO_clk_clk,                             --                          clk.clk
			ledh2f_external_connection_export   => CONNECTED_TO_ledh2f_external_connection_export,   --   ledh2f_external_connection.export
			ledlwh2f_external_connection_export => CONNECTED_TO_ledlwh2f_external_connection_export, -- ledlwh2f_external_connection.export
			memory_mem_a                        => CONNECTED_TO_memory_mem_a,                        --                       memory.mem_a
			memory_mem_ba                       => CONNECTED_TO_memory_mem_ba,                       --                             .mem_ba
			memory_mem_ck                       => CONNECTED_TO_memory_mem_ck,                       --                             .mem_ck
			memory_mem_ck_n                     => CONNECTED_TO_memory_mem_ck_n,                     --                             .mem_ck_n
			memory_mem_cke                      => CONNECTED_TO_memory_mem_cke,                      --                             .mem_cke
			memory_mem_cs_n                     => CONNECTED_TO_memory_mem_cs_n,                     --                             .mem_cs_n
			memory_mem_ras_n                    => CONNECTED_TO_memory_mem_ras_n,                    --                             .mem_ras_n
			memory_mem_cas_n                    => CONNECTED_TO_memory_mem_cas_n,                    --                             .mem_cas_n
			memory_mem_we_n                     => CONNECTED_TO_memory_mem_we_n,                     --                             .mem_we_n
			memory_mem_reset_n                  => CONNECTED_TO_memory_mem_reset_n,                  --                             .mem_reset_n
			memory_mem_dq                       => CONNECTED_TO_memory_mem_dq,                       --                             .mem_dq
			memory_mem_dqs                      => CONNECTED_TO_memory_mem_dqs,                      --                             .mem_dqs
			memory_mem_dqs_n                    => CONNECTED_TO_memory_mem_dqs_n,                    --                             .mem_dqs_n
			memory_mem_odt                      => CONNECTED_TO_memory_mem_odt,                      --                             .mem_odt
			memory_mem_dm                       => CONNECTED_TO_memory_mem_dm,                       --                             .mem_dm
			memory_oct_rzqin                    => CONNECTED_TO_memory_oct_rzqin,                    --                             .oct_rzqin
			sdram_addr                          => CONNECTED_TO_sdram_addr,                          --                        sdram.addr
			sdram_ba                            => CONNECTED_TO_sdram_ba,                            --                             .ba
			sdram_cas_n                         => CONNECTED_TO_sdram_cas_n,                         --                             .cas_n
			sdram_cke                           => CONNECTED_TO_sdram_cke,                           --                             .cke
			sdram_cs_n                          => CONNECTED_TO_sdram_cs_n,                          --                             .cs_n
			sdram_dq                            => CONNECTED_TO_sdram_dq,                            --                             .dq
			sdram_dqm                           => CONNECTED_TO_sdram_dqm,                           --                             .dqm
			sdram_ras_n                         => CONNECTED_TO_sdram_ras_n,                         --                             .ras_n
			sdram_we_n                          => CONNECTED_TO_sdram_we_n,                          --                             .we_n
			sdram_clk_clk                       => CONNECTED_TO_sdram_clk_clk,                       --                    sdram_clk.clk
			vga_interface_CLK                   => CONNECTED_TO_vga_interface_CLK,                   --                vga_interface.CLK
			vga_interface_HS                    => CONNECTED_TO_vga_interface_HS,                    --                             .HS
			vga_interface_VS                    => CONNECTED_TO_vga_interface_VS,                    --                             .VS
			vga_interface_BLANK                 => CONNECTED_TO_vga_interface_BLANK,                 --                             .BLANK
			vga_interface_SYNC                  => CONNECTED_TO_vga_interface_SYNC,                  --                             .SYNC
			vga_interface_R                     => CONNECTED_TO_vga_interface_R,                     --                             .R
			vga_interface_G                     => CONNECTED_TO_vga_interface_G,                     --                             .G
			vga_interface_B                     => CONNECTED_TO_vga_interface_B                      --                             .B
		);

