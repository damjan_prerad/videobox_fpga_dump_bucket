#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"
#include <math.h>

#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"

#include <pthread.h>

#include "ADDRESS_BASES.h"
#include "images/earth.h"
#include "images/train.h"
#include "images/test2.h"
#include "images/test3.h"
#include "images/test4.h"
#include "images/test5.h"

#define ALT_LWFPGASLVS_OFST 0xFF200000

#define HW_REGS_BASE ( 0xFC000000 )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

#define ALT_AXI_FPGASLVS_OFST (0xC0000000)
#define HW_FPGA_AXI_SPAN (0x40000000)
#define HW_FPGA_AXI_MASK ( HW_FPGA_AXI_SPAN - 1 )

pthread_t user_interface_thread;

void *addr_pixel_image1;

void *addr_lwh2f_led;
void *addr_alpha_blender;

void *addr_histogram;
void *addr_affine;

volatile char buffer_switch;

volatile uint16_t alpha;
volatile char foreground_selector = -1;
volatile char background_selector = -1;

volatile char stop = 0;

#define HISTOGRAM_R 0x0000
#define HISTOGRAM_G 0x0500
#define HISTOGRAM_B 0x0A00

#define HISTOGRAM_SIZE 255

void *user_interface_function(void* not_used)
{
	while(buffer_switch != 'q')
	{
		printf("Unesi znak 'a' za promijenu alfe.(po unesenoj alfi, broj ce biti prikazan binarno na LE diodama DE1-SoC ploce)\n");
		printf("Unesi znak 'f' za promijenu prednje slike.\n");
		printf("Unesi znak 'b' za promijenu donje slike.\n");
		printf("Unesi znak 'p' za ispis slike na ekran.\n");
		printf("Unesi znak 'h' za ispis histograma.\n");
		printf("Unesi znak 'm' za unos matrice transformacije.\n");
		printf("Unesi znak 'n' za citanje matrice transformacije.\n");
		printf("Unesi znak 'q' za izlaz iz programa.\n");
		fflush(stdin);
		fflush(stdout);
		scanf(" %c", &buffer_switch);
		
		if(buffer_switch == 'a')
		{
			printf("Unesi vrijednost alfa.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &alpha);

			//alt_write_word(addr_h2f_led + 0, a);
			//alt_write_word(addr_lwh2f_led + 0, a);
			*((uint32_t*)addr_lwh2f_led) = alpha;
			*((uint16_t*)addr_alpha_blender) = alpha;
		}
		if(buffer_switch == 'b')
		{
			printf("Unesi background selector.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &background_selector);
		}
		if(buffer_switch == 'f')
		{
			printf("Unesi foreground selector.\n");
			fflush(stdin);
			fflush(stdout);
			scanf(" %d", &foreground_selector);
		}
		if(buffer_switch == 'p')
		{
			stop = 0;
		}
		if(buffer_switch == 'h')
		{
			int i;
			for( i = 0; i < HISTOGRAM_SIZE; i++)
			{
				printf("%8d _ %8d _ %8d\n", *((uint32_t*)addr_histogram + i + HISTOGRAM_R / 4),
											*((uint32_t*)addr_histogram + i + HISTOGRAM_G / 4),
											*((uint32_t*)addr_histogram + i + HISTOGRAM_B / 4));
			}
		}
		if(buffer_switch == 'm')
		{
			int a, b;
			int elem;
			printf("Unesi velicinu matrice i,j.\n");
			scanf(" %d", &a);
			scanf(" %d", &b);
			
			int i,j;
			for( i = 0; i < a; i++)
			{
				for( j = 0; j < b; j++)
				{
					printf("Unesi element [%d][%d] :", i, j);
					scanf(" %d", &elem);
					*((uint32_t*)addr_affine + j + i * b) = elem;
				}
			}
		}
		if(buffer_switch == 'n')
		{
			int a, b;
			int elem;
			printf("Unesi velicinu matrice i,j.\n");
			scanf(" %d", &a);
			scanf(" %d", &b);
			
			int i,j;
			for( i = 0; i < a; i++)
			{
				for( j = 0; j < b; j++)
				{
					printf("%5d", *((uint32_t*)addr_affine + j + i * b));
				}
				printf("\n");
			}
		}
		if(buffer_switch == 'c')
		{
			printf("%8d\n", *((uint32_t*)addr_histogram));
			printf("%8d\n", *((uint32_t*)addr_histogram + 1));
			printf("%8d\n", *((uint32_t*)addr_histogram + 2));
			printf("%8d\n", *((uint32_t*)addr_histogram + 3));
			printf("%8d\n", *((uint32_t*)addr_histogram + 4));
			printf("%8d\n", *((uint32_t*)addr_histogram + 5));
			printf("%8d\n", *((uint32_t*)addr_histogram + 6));
			printf("%8d\n", *((uint32_t*)addr_histogram + 7));
			printf("%8d\n", *((uint32_t*)addr_histogram + 8));
			printf("%8d\n", *((uint32_t*)addr_histogram + 9));
		}
	}
}

int main(int argc, char* argv[])
{
	void*	lw_virtual_base;
	void* 	axi_virtual_base;
	
	int 	fd;

	if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		printf("ERROR: could not open \"/dev/mem\"...\n");
		
		return 1;
	}

	lw_virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, HW_REGS_BASE);
	axi_virtual_base = mmap( NULL, HW_FPGA_AXI_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, ALT_AXI_FPGASLVS_OFST );
	
	
	if (axi_virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}

	if (lw_virtual_base == MAP_FAILED) {
		printf("ERROR: mmap() failed...\n");
		close(fd);

		return 1;
	}

	addr_pixel_image1 = axi_virtual_base + ((unsigned long)(SDRAM_CONTROLLER_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));

	addr_affine = axi_virtual_base + ((unsigned long)(AFFINE_MATRIX_BASE) & (unsigned long)(HW_FPGA_AXI_MASK));

	addr_histogram = lw_virtual_base + ((unsigned long)(ALT_LWFPGASLVS_OFST + HISTOGRAM_MEMORY_BASE) & (unsigned long)(HW_REGS_MASK));
	addr_lwh2f_led = lw_virtual_base + ((unsigned long)(ALT_LWFPGASLVS_OFST + LED_INDICATION_BASE) & (unsigned long)(HW_REGS_MASK));
	addr_alpha_blender = lw_virtual_base + ((unsigned long)(ALT_LWFPGASLVS_OFST + ALPHA_BLENDER_STREAM_0_BASE) & (unsigned long)(HW_REGS_MASK));

	if(pthread_create(&user_interface_thread, NULL, user_interface_function, NULL)) {
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}

	uint16_t (*a)[480][640];
	uint16_t (*b)[480][640];

	int i, j;
	while(buffer_switch != 'q')
	{	
		uint32_t adr = 0;

		switch(foreground_selector)
		{
			case 0:
				a = train;
			break;
			case 1:
				a = earth;
			break;
			case 2:
				a = test2;
			break;
			case 3:
				a = test3;
			break;
			case 4:
				a = test4;
			break;
			case 5:
				a = test5;
			break;
			default:
				a = earth;
		}

		switch(background_selector)
		{
			case 0:
				b = train;
			break;
			case 1:
				b = earth;
			break;
			case 2:
				b = test2;
			break;
			case 3:
				b = test3;
			break;
			case 4:
				b = test4;
			break;
			case 5:
				b = test5;
			break;
			default:
				b = earth;
		}

		for(i = 0; i < 480; i++)
		{
			for(j = 0; j < 640; j++)
			{
				*((uint32_t*)addr_pixel_image1 + i * 640 + j) = ((*b)[i][j] << 16) | (*a)[i][j];
			}
		}
		while(stop == 1);
		stop = 1;
	}

	if (munmap(axi_virtual_base, HW_FPGA_AXI_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}

	if (munmap(lw_virtual_base, HW_REGS_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);

		return 1;
	}
	
	close(fd);

	return 0;
}
