

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity read_test_nowr is
    port (
-- Avalon memory mapped master for reading pixel value
			avm_m0_address     : out std_logic_vector(31 downto 0);                     -- avm_m0.address
			avm_m0_read        : out std_logic;                                        --       .read
			avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m0_write       : out std_logic;                                        --       .write
			avm_m0_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
-- Clock and reset global
			clock_clk          : in  std_logic                     := '0';             --  clock.clk
			reset_reset        : in  std_logic                     := '0';             --  reset.reset
-- Avalon mm master for writting histogram 
			avm_m1_address     : out std_logic_vector(31 downto 0);                     -- avm_m1.address
			avm_m1_read        : out std_logic;                                        --       .read
			avm_m1_waitrequest : in  std_logic                     := '0';             --       .waitrequest
			avm_m1_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
			avm_m1_write       : out std_logic;                                        --       .write
			avm_m1_writedata   : out std_logic_vector(31 downto 0)                    --       .writedata		
);
end entity read_test_nowr;

architecture rtl of read_test_nowr is

	type state_type is (s0, s1, s2, s3, S4, S5, S6, S7, s8, s9, s10, s11, s12, s13);
	signal state : state_type;
	
	signal first_buffer : std_logic_vector(31 downto 0);
	signal second_buffer : std_logic_vector(31 downto 0);
	
	signal just_a_number : integer := 1421;
	signal just_a_vector : std_logic_vector(31 downto 0) := "00000000000000000000000000001001";
	
begin
	
	process (clock_clk, reset_reset)
	begin

		if reset_reset = '1' then
			state <= s0;

		elsif (rising_edge(clock_clk)) then

			case state is
				when s0=>
					if avm_m0_waitrequest = '0' then
						state <= s1;
					else
						state <= s0;
					end if;
				when s1=>
					state <= s2;
				when s2=>
					state <= s3;
				when s3=>
					if avm_m0_waitrequest = '0' then
						state <= s4;
					else
						state <= s3;
					end if;
				when s4=>
					state <= s5;
				when s5=>
					state <= s6;
					
				when s6=>
					state <= s7;
				when s7=>
					state <= s8;
				when s8=>
					state <= s9;
				when s9=>
					state <= s10;
					
				when s10=>
					state <= s11;
				when s11=>
					state <= s12;
				when s12=>
					state <= s13;
				when s13=>
					state <= s0;
			end case;

		end if;
	end process;

	process (state)
	variable SOP_count : integer := 0;--std_logic_vector(31 downto 0) := (others => '0');
	variable EOP_count : integer := 0;--std_logic_vector(31 downto 0) := (others => '0');
	begin
			case state is
				when s0=>
					avm_m1_write <= '0';
					avm_m0_read <= '1';
					avm_m0_address <= (2 => '1', others => '0');--when 2 it doesn't work, but AB works
				when s1=>
					avm_m0_read <= '1';
					avm_m0_address <= (2 => '1', others => '0');
				when s2=>
					second_buffer <= avm_m0_readdata;
					avm_m0_read <= '0';
					avm_m0_address <= (others => '0');
				when s3 =>
					avm_m0_read <= '1';
					avm_m0_address <= (others => '0');
				when s4 =>
					avm_m0_read <= '1';
					avm_m0_address <= (others => '0');
				when s5 =>
					first_buffer <= avm_m0_readdata;
					avm_m0_read <= '0';
					if second_buffer(0) = '1' then
						SOP_count := SOP_count + 1;
						EOP_count := EOP_count + 1;
					end if;
				when s6 => null;
				when s7 => null;
				when s8 => null;
				when s9 => null;
				when s10=>
					avm_m1_write <= '1';
					avm_m1_address <= (others => '0');
					avm_m1_writedata <= first_buffer;
				when s11=>
					avm_m1_write <= '1';
					avm_m1_address <= (2 => '1', others => '0');
					avm_m1_writedata <= std_logic_vector(to_unsigned(SOP_count, avm_m1_writedata'LENGTH));
				when s12=>
					avm_m1_write <= '1';
					avm_m1_address <= (3 => '1', others => '0');
					avm_m1_writedata <= std_logic_vector(to_unsigned(EOP_count, avm_m1_writedata'LENGTH));
				when s13=>
					avm_m1_write <= '1';
					avm_m1_address <= (3 => '1', 2 => '1', others => '0');
					avm_m1_writedata <= just_a_vector;
			end case;
	end process;

end architecture rtl;
