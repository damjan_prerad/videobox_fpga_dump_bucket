--library IEEE;
--use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
--
--entity On_Chip_Writer is
--    port (
--        avm_m0_address     : out std_logic_vector(31 downto 0);                     -- avm_m0.address
--        avm_m0_read        : out std_logic;                                        --       .read
--        avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
--        avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
--        avm_m0_write       : out std_logic;                                        --       .write
--        avm_m0_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
--        clock_clk          : in  std_logic                     := '0';             --  clock.clk
--        reset_reset        : in  std_logic                     := '0';              --  reset.reset
--		  button_signal		: in std_logic_vector(9 downto 0);
--		  push_buttons			: in std_logic_vector(3 downto 0)
--    );
--end entity On_Chip_Writer;
--
--architecture rtl of On_Chip_Writer is
--signal address : integer := 0;
--signal release_check : integer := 0;
--begin
--	avm_m0_address <= std_logic_vector(to_unsigned(address,avm_m0_address'length)); --"00000000000000000000000000000000";
--	avm_m0_writedata(9 downto 0) <= button_signal;
--	
--	writing_process: process(clock_clk)
--	begin
--		if(clock_clk'event and clock_clk = '1') then	
--			if(push_buttons(0) = '0') then
--				avm_m0_write <= '1';
--			else
--				avm_m0_write <= '0';
--			end if;
--		end if;
--	end process;
--	
--	address_increment_process: process(push_buttons(1))
--	begin
--		if(push_buttons(1)'event and push_buttons(1) = '1') then	
--			if(address < 255) then
--				address <= address + 4;
--			else
--				address <= 0;
--			end if;
--		end if;
--	end process;
--end architecture rtl; -- of new_component
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity On_Chip_Writer is
    port (
        avm_m0_address     : out std_logic_vector(31 downto 0);                     -- avm_m0.address
        avm_m0_read        : out std_logic;                                        --       .read
        avm_m0_waitrequest : in  std_logic                     := '0';             --       .waitrequest
        avm_m0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --       .readdata
        avm_m0_write       : out std_logic;                                        --       .write
        avm_m0_writedata   : out std_logic_vector(31 downto 0);                    --       .writedata
        clock_clk          : in  std_logic                     := '0';             --  clock.clk
        reset_reset        : in  std_logic                     := '0';              --  reset.reset
		  button_signal		: in std_logic_vector(9 downto 0);
		  push_buttons			: in std_logic_vector(3 downto 0)
    );
end entity On_Chip_Writer;

architecture rtl of On_Chip_Writer is
signal address : integer := 0;
signal release_check : integer := 0;
begin
	avm_m0_address <= std_logic_vector(to_unsigned(address,avm_m0_address'length)); --"00000000000000000000000000000000";
	avm_m0_writedata(9 downto 0) <= button_signal;
	
	writing_process: process(clock_clk)
	begin
		if(clock_clk'event and clock_clk = '1') then	
			if(push_buttons(0) = '0') then
				avm_m0_write <= '1';
			else
				avm_m0_write <= '0';
			end if;
		end if;
	end process;
	
	address_increment_process: process(push_buttons(1))
	begin
		if(push_buttons(1)'event and push_buttons(1) = '1') then	
			if(address < 255) then
				address <= address + 4;
			else
				address <= 0;
			end if;
		end if;
	end process;
end architecture rtl; -- of new_component
